<div class="form-block <?= (isset($args['white']) && $args['white']) ? 'wow zoomIn' : ''; ?>">
	<div class="container <?= (isset($args['white']) && $args['white']) ? 'blue-form-block pt-pb-100' : '';?>">
		<div class="row justify-content-center">
			<div class="col-md-10 col-11 d-flex flex-column justify-content-center align-items-center">
				<?php if (isset($args['logo']) && $args['logo']) : ?>
					<div class="logo mb-3">
						<img src="<?= $args['logo']; ?>">
					</div>
				<?php endif;
				if (isset($args['title']) && $args['title']) : ?>
					<h2 class="base-title form-title"><?= $args['title']; ?></h2>
				<?php endif;
				if (isset($args['offer']) && $args['offer']) : ?>
					<p class="foo-offer"><?= $args['offer']; ?></p>
				<?php endif;
				if (isset($args['text']) && $args['text']) : ?>
					<p class="base-text form-text"><?= $args['text']; ?></p>
				<?php endif;
				getForm('30'); ?>
			</div>
		</div>
	</div>
</div>
