<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<div class="offer-line open-form sticky">
	<img src="<?= ICONS ?>pop-trigger.png">
	<span class="offer-text">
		קבל הצעת  מחיר עכשיו
	</span>
</div>
<?php if ($video = $fields['main_video']) : ?>
<div class="gen-video-block">
	<div class="in-video-text">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-7 col-lg-8 col-md-7 col-10 d-flex flex-column justify-content-center align-items-center">
					<?php if ($logo = opt('logo_white_single')) : ?>
						<div class="logo">
							<img src="<?= $logo['url']; ?>">
						</div>
					<?php endif;
					if ($fields['main_text']) : ?>
						<h2 class="main-text"><?= $fields['main_text']; ?></h2>
					<?php endif; ?>
					<div class="pop-trigger open-form">
						<img src="<?= ICONS ?>pop-trigger.png">
						<span class="offer-text">
								קבל הצעת  מחיר עכשיו
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<video muted autoplay="autoplay" loop="loop">
		<source src="<?= $video['url']; ?>" type="video/mp4">
		<source src="<?= $video['url']; ?>" type="video/ogg">
	</video>
</div>
<?php endif;
if ($fields['benefit_item']) : ?>
	<section class="benefits pt-pb-100">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php foreach ($fields['benefit_item'] as $num => $benefit) : ?>
						<div class="row justify-content-center align-items-stretch benefit-row">
							<div class="col-lg-6 col-12 benefit-col wow fadeInDown">
								<div class="benefit-text-wrap">
									<div class="base-output">
										<?= $benefit['benefit_text']; ?>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-12 img-col wow fadeInUp" <?php if ($benefit['benefit_img']) : ?>
								style="background-image: url('<?= $benefit['benefit_img']['url']; ?>')"
								<?php endif; ?>>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form', [
		'logo' => opt('logo_blue') ? opt('logo_blue')['url'] : '',
		'white' => true,
		'title' => $fields['mid_form_title'] ? $fields['mid_form_title'] : 'הגיע הזמן לעלות ביידים טובות - השאירו פרטים עכשיו',
		'text' => $fields['mid_form_text'] ? $fields['mid_form_text'] : 'הצטרפו למשפחת אלקטרה לעלול קומה בצורה בטוחה עם אחריות מלאה',
]);
if ($fields['counter_item']) : ?>
	<div class="counter-block pt-pb-100">
		<div class="container" id="start-count">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="row justify-content-center">
						<div class="col-lg-3 col-6 count-col">
							<div class="counter-item">
								<div class="counter-icon-wrap counter-year" <?php if ($symbol = opt('symbol')) : ?>
									style="background-image: url('<?= $symbol['url']; ?>')"
								<?php endif; ?>>
									<h2 class="counter-number counter-num">
										1956
									</h2>
								</div>
								<h4 class="why-us-text"> 65 שנות ניסיון</h4>
							</div>
						</div>
						<?php foreach ($fields['counter_item'] as $count) : ?>
							<div class="col-lg-3 col-6 count-col">
								<div class="counter-item">
									<div class="counter-icon-wrap" <?php if ($symbol = opt('symbol')) : ?>
										style="background-image: url('<?= $symbol['url']; ?>')"
									<?php endif; ?>>
										<h2 class="timer counter-number counter-num" data-from="0"
											data-to="<?= $count['counter_num']; ?>" data-speed="1500">
										</h2>
									</div>
									<h4 class="why-us-text"><?= $count['counter_title']; ?></h4>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif;
if ($text = $fields['text_content']) : ?>
	<section class="content-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="row align-items-center">
						<div class="col">
							<div class="base-output">
								<?= $text; ?>
							</div>
						</div>
						<div class="<?= $fields['content_img'] ? 'col-lg-6' : 'col-12'; ?> img-content-col">
							<?php if ($fields['content_img']) : ?>
								<img src="<?= $fields['content_img']['url']; ?>">
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['gallery_item']) :
	$gal = makeGallery($fields['gallery_item']); ?>
	<section class="gallery-block mt-5">
		<div class="container">
			<?php if ($fields['gallery_title']) : ?>
				<div class="row">
					<div class="col">
						<h2 class="gallery-title"><?= $fields['gallery_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row align-items-stretch">
				<?php foreach ($gal as $x => $col) : ?>
					<div class="col-lg-4 col-12 column-gal wow fadeInDown" data-wow-delay="0.<?= $x + 4; ?>s">
						<?php foreach ($col as $item) : ?>
							<a class="item-gal" <?php if ($item['gallery_item_img']) : ?>
								style="background-image: url('<?= $item['gallery_item_img']['url']; ?>')"
								href="<?= $item['gallery_item_img']['url']; ?>"
							<?php endif; ?> data-lightbox="gallery">
									<span class="gallery-overlay">
										<h3 class="gal-item-title"><?= $item['gallery_item_title']; ?></h3>
										<p class="gal-item-text"><?= $item['gallery_item_text']; ?></p>
									</span>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['why_block_text'] || $fields['why_item']) : ?>
	<section class="why-us-block pt-pb-100">
		<div class="container">
			<?php if ($fields['why_block_text']) : ?>
				<div class="row justify-content-center">
					<div class="col block-description">
						<div class="base-output">
							<?= $fields['why_block_text']; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['why_item']) : ?>
				<div class="row justify-content-center align-items-start">
					<?php foreach ($fields['why_item'] as $y => $ben_item) : ?>
						<div class="col-xl-4 col-sm-6 col-12 mb-5 wow fadeIn" data-wow-delay="0.<?= $y + 2; ?>s">
							<div class="benefit-item">
								<div class="benefit-icon">
									<?php if ($ben_item['why_icon']) : ?>
										<img src="<?= $ben_item['why_icon']['url']; ?>" alt="icon">
									<?php endif; ?>
								</div>
								<h3 class="benefit-title"><?= $ben_item['why_title']; ?></h3>
								<p class="benefit-text"><?= $ben_item['why_text']; ?></p>
							</div>
						</div>
						<?php if ($y == '2') : ?>
							<div class="col-12 d-flex justify-content-center mb-5">
								<div class="middle-link open-form">
									תנו לי הצעה עכשיו
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
get_footer(); ?>
