<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');


?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<span class="arrow-top-wrap">
				<img src="<?= ICONS ?>to-top.png">
			</span>
			<h5 class="to-top-text">חזרה למעלה</h5>
		</a>
		<?php
		get_template_part('views/partials/repeat', 'form', [
				'logo' => opt('logo_blue') ? opt('logo_white')['url'] : '',
				'white' => false,
				'offer' => opt('foo_form_text'),
		]);
		?>
	</div>
</footer>
<div id="leos">
    <a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
        <img src="<?= IMG . 'leos_logo.png' ?>"
             alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
        <span></span>
    </a>
</div>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
    require_once THEMEPATH . "/inc/debug.php"
    ?>
    <script>

        function _fetchHeader($_el){
            let res = {
                'count' : 0,
                'content' : ''
            } ;
            $($_el).each(function () {
                res.count++;
                res.content += ' [' + $(this).text() + '] ';
            });
            return 'Count: ' + res.count + '. Text: ' + res.content;
        }

        function _fetchMeta($_meta){
            return $('meta[name='+$_meta+']').attr("content");
        }




        phpdebugbar.addDataSet({
            "SEO Local": {
                'H1' : _fetchHeader('h1'),
                'H2' : _fetchHeader('h2'),
                'H3' : _fetchHeader('h3'),
                'Meta Title' : _fetchMeta('title'),
                'Meta Description' : _fetchMeta('description'),
                'Meta Keywords' : _fetchMeta('keywords'),
            }
        });
    </script>

<?php endif; ?>

</body>
</html>
