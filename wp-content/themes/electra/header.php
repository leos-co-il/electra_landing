<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-auto">
				<?php if ($tel = opt('tel')) : ?>
					<a href="tel:<?= $tel; ?>" class="header-tel">
						<span class="tel-number"><?= $tel; ?></span>
						<?= svg_simple(ICONS.'tel.svg'); ?>
					</a>
					<a href="tel:<?= $tel; ?>" class="fixed-tel">
						<?= svg_simple(ICONS.'tel.svg'); ?>
					</a>
				<?php endif; ?>
            </div>
        </div>
    </div>
</header>

<section class="pop-form">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-9 col-12 d-flex justify-content-center">
				<div class="float-form blue-form-block">
					<span class="pop-close">
						<?= svg_simple(ICONS.'close.svg'); ?>
					</span>
					<div class="d-flex flex-column justify-content-center align-items-center">
						<?php if ($logo = opt('logo_blue')) : ?>
							<div class="logo mb-3">
								<img src="<?= $logo['url']; ?>">
							</div>
						<?php endif;
						if ($title = opt('pop_title')) : ?>
							<h2 class="base-title form-title"><?= $title; ?></h2>
						<?php endif;
						if ($text = opt('pop_text')) : ?>
							<p class="base-text form-text"><?= $text; ?></p>
						<?php endif;
						getForm('30'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
