(function($) {

	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		$('.open-form').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-form');
			$('body').addClass('body-hidden');
		});
		$('.pop-close').click(function () {
			$('.float-form').removeClass('show-form');
			$('.pop-form').removeClass('show-popup');
			$('body').removeClass('body-hidden');
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		// $('.play-button').click(function() {
		// 	var id = $(this).data('video');
		// 	var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
		// 	$('#iframe-wrapper').html(frame);
		// 	$('#modalCenter').modal('show');
		// });
		// $('#modalCenter').on('hidden.bs.modal', function (e) {
		// 	$('#iframe-wrapper').html('');
		// });
	});


})( jQuery );
