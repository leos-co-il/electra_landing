-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 30, 2021 at 02:29 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electra`
--

-- --------------------------------------------------------

--
-- Table structure for table `fmn_commentmeta`
--

CREATE TABLE `fmn_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_comments`
--

CREATE TABLE `fmn_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_comments`
--

INSERT INTO `fmn_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'מגיב וורדפרס', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-04-29 12:58:11', '2021-04-29 09:58:11', 'היי, זו תגובה.\nכדי לשנות, לערוך, או למחוק תגובות, יש לגשת למסך התגובות בלוח הבקרה.\nצלמית המשתמש של המגיב מגיעה מתוך <a href=\"https://gravatar.com\">גראווטר</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_links`
--

CREATE TABLE `fmn_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_options`
--

CREATE TABLE `fmn_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_options`
--

INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://electra:8888', 'yes'),
(2, 'home', 'http://electra:8888', 'yes'),
(3, 'blogname', 'electra', 'yes'),
(4, 'blogdescription', 'אתר וורדפרס חדש', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'maxf@leos.co.il', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j בF Y', 'yes'),
(24, 'time_format', 'G:i', 'yes'),
(25, 'links_updated_date_format', 'j בF Y G:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:97:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=5&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:33:\"jquery-updater/jquery-updater.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'electra', 'yes'),
(41, 'stylesheet', 'electra', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '49752', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Asia/Jerusalem', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '5', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1635242291', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'fmn_user_roles', 'a:6:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:11:\"leos_client\";a:2:{s:4:\"name\";s:11:\"Leos Client\";s:12:\"capabilities\";a:124:{s:16:\"activate_plugins\";b:0;s:19:\"delete_others_pages\";b:1;s:19:\"delete_others_posts\";b:1;s:12:\"delete_pages\";b:1;s:12:\"delete_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:14:\"edit_dashboard\";b:1;s:17:\"edit_others_pages\";b:1;s:17:\"edit_others_posts\";b:1;s:10:\"edit_pages\";b:1;s:10:\"edit_posts\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_theme_options\";b:1;s:6:\"export\";b:0;s:6:\"import\";b:0;s:10:\"list_users\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:13:\"promote_users\";b:0;s:13:\"publish_pages\";b:1;s:13:\"publish_posts\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:4:\"read\";b:1;s:12:\"remove_users\";b:0;s:13:\"switch_themes\";b:0;s:12:\"upload_files\";b:1;s:11:\"update_core\";b:0;s:14:\"update_plugins\";b:0;s:13:\"update_themes\";b:0;s:15:\"install_plugins\";b:0;s:14:\"install_themes\";b:0;s:13:\"delete_themes\";b:0;s:14:\"delete_plugins\";b:0;s:12:\"edit_plugins\";b:0;s:11:\"edit_themes\";b:0;s:10:\"edit_files\";b:0;s:10:\"edit_users\";b:0;s:9:\"add_users\";b:0;s:12:\"create_users\";b:0;s:12:\"delete_users\";b:0;s:15:\"unfiltered_html\";b:1;s:18:\"manage_woocommerce\";b:1;s:25:\"manage_woocommerce_orders\";b:1;s:26:\"manage_woocommerce_coupons\";b:1;s:27:\"manage_woocommerce_products\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:10:\"copy_posts\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:17:\"edit_shop_webhook\";b:1;s:17:\"read_shop_webhook\";b:1;s:19:\"delete_shop_webhook\";b:1;s:18:\"edit_shop_webhooks\";b:1;s:25:\"edit_others_shop_webhooks\";b:1;s:21:\"publish_shop_webhooks\";b:1;s:26:\"read_private_shop_webhooks\";b:1;s:20:\"delete_shop_webhooks\";b:1;s:28:\"delete_private_shop_webhooks\";b:1;s:30:\"delete_published_shop_webhooks\";b:1;s:27:\"delete_others_shop_webhooks\";b:1;s:26:\"edit_private_shop_webhooks\";b:1;s:28:\"edit_published_shop_webhooks\";b:1;s:25:\"manage_shop_webhook_terms\";b:1;s:23:\"edit_shop_webhook_terms\";b:1;s:25:\"delete_shop_webhook_terms\";b:1;s:25:\"assign_shop_webhook_terms\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'he_IL', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:8:{i:1619783892;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1619819892;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1619819937;a:1:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1619863092;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1619863100;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1619863101;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1620381537;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(123, '_site_transient_timeout_browser_291da66cbb2b7ebec14fe3e09324e087', '1620295100', 'no'),
(124, '_site_transient_browser_291da66cbb2b7ebec14fe3e09324e087', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"90.0.4430.85\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(125, '_site_transient_timeout_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', '1620295101', 'no'),
(126, '_site_transient_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(138, 'disallowed_keys', '', 'no'),
(139, 'comment_previously_approved', '1', 'yes'),
(140, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(141, 'auto_update_core_dev', 'enabled', 'yes'),
(142, 'auto_update_core_minor', 'enabled', 'yes'),
(143, 'auto_update_core_major', 'unset', 'yes'),
(144, 'finished_updating_comment_type', '1', 'yes'),
(145, 'db_upgraded', '', 'yes'),
(149, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:26:\"בקשת HTTPS נכשלה.\";}}', 'yes'),
(150, 'can_compress_scripts', '1', 'no'),
(151, 'theme_mods_twentytwenty', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1619690363;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(152, 'current_theme', 'Electra', 'yes'),
(153, 'theme_mods_twentytwentyone', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1619690366;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(154, 'theme_switched', '', 'yes'),
(156, 'theme_mods_electra', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(158, 'recently_activated', 'a:0:{}', 'yes'),
(169, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.4.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1619691271;s:7:\"version\";s:5:\"5.4.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(170, 'acf_version', '5.9.5', 'yes'),
(172, 'options_tel', '1800-65-63-65', 'no'),
(173, '_options_tel', 'field_5dd3a33f42e0c', 'no'),
(174, 'options_mail', '', 'no'),
(175, '_options_mail', 'field_5dd3a35642e0d', 'no'),
(176, 'options_fax', '', 'no'),
(177, '_options_fax', 'field_5dd3a36942e0e', 'no'),
(178, 'options_address', '', 'no'),
(179, '_options_address', 'field_5dd3a4d142e0f', 'no'),
(180, 'options_map_image', '', 'no'),
(181, '_options_map_image', 'field_5ddbd67734310', 'no'),
(182, 'options_facebook', '', 'no'),
(183, '_options_facebook', 'field_5ddbd6b3cc0cd', 'no'),
(184, 'options_whatsapp', '', 'no'),
(185, '_options_whatsapp', 'field_5ddbd6cdcc0ce', 'no'),
(186, 'options_logo_blue', '32', 'no'),
(187, '_options_logo_blue', 'field_608a8ab99ed9a', 'no'),
(188, 'options_symbol', '33', 'no'),
(189, '_options_symbol', 'field_608a8ae69ed9b', 'no'),
(191, 'options_foo_form_text', 'מעלית מעלון? לנו יש פיתרון לכל צורך שתבחרו, השאר פרטים לאלקטרה ונחזור בהקדם:', 'no'),
(192, '_options_foo_form_text', 'field_608aafde239d1', 'no'),
(196, '_transient_timeout_acf_plugin_info_pro', '1619791857', 'no'),
(197, '_transient_acf_plugin_info_pro', 'a:18:{s:4:\"name\";s:26:\"Advanced Custom Fields PRO\";s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:7:\"version\";s:5:\"5.9.5\";s:8:\"homepage\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"author\";s:64:\"<a href=\"https://www.advancedcustomfields.com\">Elliot Condon</a>\";s:12:\"contributors\";a:1:{s:12:\"elliotcondon\";a:3:{s:7:\"profile\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"avatar\";s:81:\"https://secure.gravatar.com/avatar/c296f449f92233e8d1102ff01c9bc71e?s=96&d=mm&r=g\";s:12:\"display_name\";s:22:\"Advanced Custom Fields\";}}s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:3:\"5.6\";s:6:\"tested\";s:3:\"5.7\";s:5:\"added\";s:10:\"2014-07-11\";s:12:\"last_updated\";s:10:\"2021-04-06\";s:11:\"description\";s:1349:\"<p>Use the Advanced Custom Fields plugin to take full control of your WordPress edit screens & custom field data.</p>\n\n<p><strong>Add fields on demand.</strong> Our field builder allows you to quickly and easily add fields to WP edit screens with only the click of a few buttons!</p>\n\n<p><strong>Add them anywhere.</strong> Fields can be added all over WP including posts, users, taxonomy terms, media, comments and even custom options pages!</p>\n\n<p><strong>Show them everywhere.</strong> Load and display your custom field values in any theme template file with our hassle free developer friendly functions!</p>\n\n<h4>Features</h4>\n<ul>\n<li>Simple & Intuitive</li>\n<li>Powerful Functions</li>\n<li>Over 30 Field Types</li>\n<li>Extensive Documentation</li>\n<li>Millions of Users</li>\n</ul>\n\n<h4>Links</h4>\n<ul>\n<li><a href=\"https://www.advancedcustomfields.com\">Website</a></li>\n<li><a href=\"https://www.advancedcustomfields.com/resources/\">Documentation</a></li>\n<li><a href=\"https://support.advancedcustomfields.com\">Support</a></li>\n<li><a href=\"https://www.advancedcustomfields.com/pro/\">ACF PRO</a></li>\n</ul>\n\n<h4>PRO</h4>\n<p>The Advanced Custom Fields plugin is also available in a professional version which includes more fields, more functionality, and more flexibility! <a href=\"https://www.advancedcustomfields.com/pro/\">Learn more</a></p>\";s:12:\"installation\";s:503:\"<p>From your WordPress dashboard</p>\n\n<ol>\n<li><strong>Visit</strong> Plugins > Add New</li>\n<li><strong>Search</strong> for \"Advanced Custom Fields\"</li>\n<li><strong>Activate</strong> Advanced Custom Fields from your Plugins page</li>\n<li><strong>Click</strong> on the new menu item \"Custom Fields\" and create your first Custom Field Group!</li>\n<li><strong>Read</strong> the documentation to <a href=\"https://www.advancedcustomfields.com/resources/getting-started-with-acf/\">get started</a></li>\n</ol>\";s:9:\"changelog\";s:5158:\"<h4>5.9.5</h4>\n<p><em>Release Date - 11 February 2021</em></p>\n\n<ul>\n<li>Fix - Fixed regression preventing blocks from loading correctly within the editor in WordPress 5.5.</li>\n<li>Fix - Fixed bug causing incorrect post_status properties when restoring a Field Group from trash in WordPress 5.6.</li>\n<li>Fix - Fixed edge case bug where a taxonomy named \"options\" could interfere with saving and loading option values.</li>\n<li>Fix - Fixed additional PHP 8.0 warnings.</li>\n<li>i18n - Updated Finnish translation thanks to Mikko Kekki</li>\n</ul>\n\n<h4>5.9.4</h4>\n<p><em>Release Date - 14 January 2021</em></p>\n\n<ul>\n<li>Enhancement - Added PHP validation for the Email field (previously relied solely on browser validation).</li>\n<li>Fix - Added support for PHP 8.0 (fixed logged warnings).</li>\n<li>Fix - Added support for jQuery 3.5 (fixed logged warnings).</li>\n<li>Fix - Fixed bug causing WYSIWYG field to appear unresponsive within the Gutenberg editor.</li>\n<li>Fix - Fixed regression preventing \"blog_%d\" and \"site_%d\" as valid <code>$post_id</code> values for custom Taxonomy terms.</li>\n<li>Fix - Fixed bug causing Radio field label to select first choice.</li>\n<li>Fix - Fixed bug preventing preloading blocks that contain multiple parent DOM elements.</li>\n<li>i18n - Updated Japanese translation thanks to Ryo Takahashi.</li>\n<li>i18n - Updated Portuguese translation thanks to Pedro Mendonça.</li>\n</ul>\n\n<h4>5.9.3</h4>\n<p><em>Release Date - 3 November 2020</em></p>\n\n<ul>\n<li>Fix - Fixed bug causing Revision meta to incorrectly update the parent Post meta.</li>\n<li>Fix - Fixed bug breaking \"Filter by Post Type\" and \"Filter by Taxonomy\" Field settings.</li>\n</ul>\n<p>* Added toolbar across all ACF admin pages.</p>\n<p>* Added new table columns: Description, Key, Location, Local JSON.</p>\n<p>* Added popup modal to review Local JSON changes before sync.</p>\n<p>* Added visual representation of where Field Groups will appear.</p>\n<p>* Added new help tab.</p>\n<p>* Simplified layout.</p>\n<ul>\n<li>Enhancement - New ACF Blocks features.</li>\n</ul>\n<p>* Added support for Inner Blocks.</p>\n<p>* Added new \"jsx\" setting.</p>\n<p>* Added new \"align_text\" settings.</p>\n<p>* Added new \"align_content\" settings.</p>\n<ul>\n<li>Enhancement - Added duplicate functionality for Repeater and Flexible Content fields.</li>\n<li>Enhancement - Added PHP validation support for Gutenberg.</li>\n<li>Enhancement - Added ability to bypass confirmation tooltips (just hold shift).</li>\n<li>Enhancement - Local JSON files now save back to their loaded source path (not \"save_json\" setting).</li>\n<li>Tweak - Replaced all custom icons with dashicons.</li>\n<li>Tweak - Changed custom post status label from \"Inactive\" to \"Disabled\".</li>\n<li>Tweak - Improved styling of metaboxes positioned in the block editor sidebar.</li>\n<li>Fix - Improved AJAX request efficiency when editing block className or anchor attributes.</li>\n<li>Fix - Fixed bug causing unresponsive WYSIWYG fields after moving a block via the up/down arrows.</li>\n<li>Fix - Fixed bug causing HTML to jump between multiple instances of the same Reusable Block.</li>\n<li>Fix - Fixed bug sometimes displaying validation errors when saving a draft.</li>\n<li>Fix - Fixed bug breaking Image field UI when displaying a scaled portrait attachment.</li>\n<li>Fix - Fixed bug in Link field incorrectly treating the \"Cancel\" button as \"Submit\".</li>\n<li>Fix - Fixed bug where a sub field within a collapsed Repeater row did not grow to the full available width.</li>\n<li>Fix - Ensured all archive URLs shown in the Page Link field dropdown are unique.</li>\n<li>Fix - Fixed bug causing incorrect conditional logic settings on nested fields when duplicating a Field Group.</li>\n<li>Fix - Fixed bug causing license activation issues with some password management browser extensions.</li>\n<li>Dev - Major improvements to <code>ACF_Location</code> class.</li>\n<li>Dev - Refactored all location classes to optimize performance.</li>\n<li>Dev - Extracted core JavaScript from \"acf-input.js\" into a separate \"acf.js\" file.</li>\n<li>Dev - Field Group export now shows \"active\" attribute as bool instead of int.</li>\n<li>Dev - Added filter \"acf/get_object_type\" to customize WP object information such as \"label\" and \"icon\".</li>\n<li>Dev - Added action \"acf/admin_print_uploader_scripts\" fired when printing uploader (WP media) scripts in the footer.</li>\n<li>Dev - Added filters \"acf/pre_load_attachment\" and \"acf/load_attachment\" to customize attachment details.</li>\n<li>Dev - Added filter \"acf/admin/toolbar\" to customize the admin toolbar items.</li>\n<li>Dev - Added new JS actions \"duplicate_fields\" and \"duplicate_field\" fired when duplicating a row.</li>\n<li>i18n - Changed Croatian locale code from \"hr_HR to \"hr\".</li>\n<li>i18n - Updated Portuguese translation thanks to Pedro Mendonça.</li>\n<li>i18n - Updated French Canadian translation thanks to Bérenger Zyla.</li>\n<li>i18n - Updated French translation thanks to Maxime Bernard-Jacquet.</li>\n<li>i18n - Updated German translations thanks to Ralf Koller.</li>\n</ul>\n\n<p><a href=\"https://www.advancedcustomfields.com/changelog/\">View the full changelog</a></p>\";s:14:\"upgrade_notice\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:8:\"versions\";a:122:{i:0;s:5:\"5.9.4\";i:1;s:5:\"5.9.3\";i:2;s:5:\"5.9.2\";i:3;s:5:\"5.9.1\";i:4;s:11:\"5.9.0-beta5\";i:5;s:11:\"5.9.0-beta4\";i:6;s:11:\"5.9.0-beta3\";i:7;s:11:\"5.9.0-beta2\";i:8;s:11:\"5.9.0-beta1\";i:9;s:9:\"5.9.0-RC1\";i:10;s:5:\"5.9.0\";i:11;s:5:\"5.8.9\";i:12;s:5:\"5.8.8\";i:13;s:5:\"5.8.7\";i:14;s:5:\"5.8.6\";i:15;s:5:\"5.8.5\";i:16;s:5:\"5.8.4\";i:17;s:5:\"5.8.3\";i:18;s:5:\"5.8.2\";i:19;s:6:\"5.8.14\";i:20;s:6:\"5.8.13\";i:21;s:6:\"5.8.12\";i:22;s:6:\"5.8.11\";i:23;s:5:\"5.8.1\";i:24;s:13:\"5.8.0-beta4.1\";i:25;s:11:\"5.8.0-beta4\";i:26;s:11:\"5.8.0-beta3\";i:27;s:11:\"5.8.0-beta2\";i:28;s:11:\"5.8.0-beta1\";i:29;s:9:\"5.8.0-RC2\";i:30;s:9:\"5.8.0-RC1\";i:31;s:5:\"5.8.0\";i:32;s:5:\"5.7.9\";i:33;s:5:\"5.7.8\";i:34;s:5:\"5.7.7\";i:35;s:5:\"5.7.6\";i:36;s:5:\"5.7.5\";i:37;s:5:\"5.7.4\";i:38;s:5:\"5.7.3\";i:39;s:5:\"5.7.2\";i:40;s:6:\"5.7.13\";i:41;s:6:\"5.7.12\";i:42;s:6:\"5.7.10\";i:43;s:5:\"5.7.1\";i:44;s:5:\"5.7.0\";i:45;s:5:\"5.6.9\";i:46;s:5:\"5.6.8\";i:47;s:5:\"5.6.7\";i:48;s:5:\"5.6.6\";i:49;s:5:\"5.6.5\";i:50;s:5:\"5.6.4\";i:51;s:5:\"5.6.3\";i:52;s:5:\"5.6.2\";i:53;s:6:\"5.6.10\";i:54;s:5:\"5.6.1\";i:55;s:11:\"5.6.0-beta2\";i:56;s:11:\"5.6.0-beta1\";i:57;s:9:\"5.6.0-RC2\";i:58;s:9:\"5.6.0-RC1\";i:59;s:5:\"5.6.0\";i:60;s:5:\"5.5.9\";i:61;s:5:\"5.5.7\";i:62;s:5:\"5.5.5\";i:63;s:5:\"5.5.3\";i:64;s:5:\"5.5.2\";i:65;s:6:\"5.5.14\";i:66;s:6:\"5.5.13\";i:67;s:6:\"5.5.12\";i:68;s:6:\"5.5.11\";i:69;s:6:\"5.5.10\";i:70;s:5:\"5.5.1\";i:71;s:5:\"5.5.0\";i:72;s:5:\"5.4.8\";i:73;s:5:\"5.4.7\";i:74;s:5:\"5.4.6\";i:75;s:5:\"5.4.5\";i:76;s:5:\"5.4.4\";i:77;s:5:\"5.4.3\";i:78;s:5:\"5.4.2\";i:79;s:5:\"5.4.1\";i:80;s:5:\"5.4.0\";i:81;s:5:\"5.3.9\";i:82;s:5:\"5.3.8\";i:83;s:5:\"5.3.7\";i:84;s:5:\"5.3.6\";i:85;s:5:\"5.3.5\";i:86;s:5:\"5.3.4\";i:87;s:5:\"5.3.3\";i:88;s:5:\"5.3.2\";i:89;s:6:\"5.3.10\";i:90;s:5:\"5.3.1\";i:91;s:5:\"5.3.0\";i:92;s:5:\"5.2.9\";i:93;s:5:\"5.2.8\";i:94;s:5:\"5.2.7\";i:95;s:5:\"5.2.6\";i:96;s:5:\"5.2.5\";i:97;s:5:\"5.2.4\";i:98;s:5:\"5.2.3\";i:99;s:5:\"5.2.2\";i:100;s:5:\"5.2.1\";i:101;s:5:\"5.2.0\";i:102;s:5:\"5.1.9\";i:103;s:5:\"5.1.8\";i:104;s:5:\"5.1.7\";i:105;s:5:\"5.1.6\";i:106;s:5:\"5.1.5\";i:107;s:5:\"5.1.4\";i:108;s:5:\"5.1.3\";i:109;s:5:\"5.1.2\";i:110;s:5:\"5.1.1\";i:111;s:5:\"5.1.0\";i:112;s:5:\"5.0.9\";i:113;s:5:\"5.0.8\";i:114;s:5:\"5.0.7\";i:115;s:5:\"5.0.6\";i:116;s:5:\"5.0.5\";i:117;s:5:\"5.0.4\";i:118;s:5:\"5.0.3\";i:119;s:5:\"5.0.2\";i:120;s:5:\"5.0.1\";i:121;s:5:\"5.0.0\";}}', 'no'),
(198, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TmpRNE5USjhkSGx3WlQxd1pYSnpiMjVoYkh4a1lYUmxQVEl3TVRVdE1Ea3RNak1nTURVNk1qQTZNVFE9IjtzOjM6InVybCI7czoxOToiaHR0cDovL2VsZWN0cmE6ODg4OCI7fQ==', 'yes'),
(199, '_transient_timeout_acf_plugin_updates', '1619881045', 'no'),
(200, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.5\";}}', 'no'),
(206, 'options_logo_white', '88', 'no'),
(207, '_options_logo_white', 'field_608a8a969ed99', 'no'),
(208, 'options_logo_white_single', '31', 'no'),
(209, '_options_logo_white_single', 'field_608ac9250d400', 'no'),
(219, '_site_transient_timeout_theme_roots', '1619779146', 'no'),
(220, '_site_transient_theme_roots', 'a:2:{s:7:\"electra\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no'),
(221, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.7.1.zip\";s:6:\"locale\";s:5:\"he_IL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.7.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.1\";s:7:\"version\";s:5:\"5.7.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1619777348;s:15:\"version_checked\";s:5:\"5.7.1\";s:12:\"translations\";a:0:{}}', 'no'),
(222, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1619777349;s:7:\"checked\";a:2:{s:7:\"electra\";s:3:\"1.0\";s:15:\"twentytwentyone\";s:3:\"1.3\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:1:{s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.3.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}', 'no'),
(223, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1619777350;s:7:\"checked\";a:12:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.5\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.6\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.4.1\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:7:\"1.2.5.9\";s:24:\"fancy-gallery/plugin.php\";s:6:\"1.6.56\";s:33:\"jquery-updater/jquery-updater.php\";s:7:\"3.6.0.1\";s:29:\"versionpress/versionpress.php\";s:9:\"4.0-beta2\";s:17:\"wesafe/wesafe.php\";s:5:\"1.7.2\";s:25:\"dummy-data/dummy-data.php\";s:3:\"1.0\";s:29:\"wp-sync-db-1.5/wp-sync-db.php\";s:3:\"1.5\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"16.2\";s:17:\"leosem/leosem.php\";s:3:\"2.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2021-02-05 18:15:06\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.4.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:4:\"16.2\";s:7:\"updated\";s:19:\"2021-03-22 15:34:46\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/16.2/he_IL.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:6:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.4.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:7:\"1.2.5.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"fancy-gallery/plugin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/fancy-gallery\";s:4:\"slug\";s:13:\"fancy-gallery\";s:6:\"plugin\";s:24:\"fancy-gallery/plugin.php\";s:11:\"new_version\";s:6:\"1.6.56\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/fancy-gallery/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/fancy-gallery.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/fancy-gallery/assets/icon-128x128.png?rev=1723946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/fancy-gallery/assets/banner-772x250.png?rev=1723946\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"jquery-updater/jquery-updater.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/jquery-updater\";s:4:\"slug\";s:14:\"jquery-updater\";s:6:\"plugin\";s:33:\"jquery-updater/jquery-updater.php\";s:11:\"new_version\";s:7:\"3.6.0.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/jquery-updater/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/jquery-updater.3.6.0.1.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:58:\"https://s.w.org/plugins/geopattern-icon/jquery-updater.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"16.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.16.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no'),
(224, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":7,\"critical\":1}', 'yes'),
(226, 'options_pop_title', 'הגיע הזמן לעלות ביידים טובות - השאירו פרטים עכשיו', 'no'),
(227, '_options_pop_title', 'field_608be561a6ce7', 'no'),
(228, 'options_pop_text', 'הצטרפו למשפחת אלקטרה לעלול קומה בצורה בטוחה עם אחריות מלאה', 'no'),
(229, '_options_pop_text', 'field_608be566a6ce8', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_postmeta`
--

CREATE TABLE `fmn_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_postmeta`
--

INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1619780808:1'),
(4, 5, '_wp_page_template', 'views/home.php'),
(11, 8, '_edit_last', '1'),
(12, 8, '_edit_lock', '1619778241:1'),
(25, 26, '_edit_last', '1'),
(26, 26, '_edit_lock', '1619708546:1'),
(27, 17, '_edit_lock', '1619692718:1'),
(28, 30, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-xl-3 col-md-6 col-12\">\n		[text* text-173 placeholder \"שם:\"]\n	</div>\n    <div class=\"col-xl-3 col-md-6 col-12\">\n			[tel* tel-48 placeholder \"טלפון:\"]\n	</div>\n	 <div class=\"col-xl-6 col-12\">\n		 [select menu-648 \"אני מעוניין שיחזרו אלי לגביי הפנייה הבאה\"]\n	</div>\n    <div class=\"col-12 submit-col\">\n			[submit \"אני רוצה הצעת מחיר מאלקטרה  עכשיו\"]\n	</div>\n</div>'),
(29, 30, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:33:\"[_site_title] <wordpress@electra>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(30, 30, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:33:\"[_site_title] <wordpress@electra>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(31, 30, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(32, 30, '_additional_settings', ''),
(33, 30, '_locale', 'he_IL'),
(36, 5, '_edit_last', '1'),
(37, 5, 'title_tag', ''),
(38, 5, '_title_tag', 'field_5ddbe7577e0e7'),
(39, 5, 'single_slider_seo', ''),
(40, 5, '_single_slider_seo', 'field_5ddbde5499115'),
(41, 5, 'single_gallery', ''),
(42, 5, '_single_gallery', 'field_5ddbe5aea4275'),
(43, 5, 'single_video_slider', ''),
(44, 5, '_single_video_slider', 'field_5ddbe636a4276'),
(45, 6, 'title_tag', ''),
(46, 6, '_title_tag', 'field_5ddbe7577e0e7'),
(47, 6, 'single_slider_seo', ''),
(48, 6, '_single_slider_seo', 'field_5ddbde5499115'),
(49, 6, 'single_gallery', ''),
(50, 6, '_single_gallery', 'field_5ddbe5aea4275'),
(51, 6, 'single_video_slider', ''),
(52, 6, '_single_video_slider', 'field_5ddbe636a4276'),
(53, 9, '_wp_trash_meta_status', 'publish'),
(54, 9, '_wp_trash_meta_time', '1619700867'),
(55, 9, '_wp_desired_post_slug', 'group_5ddbde4d59412'),
(56, 10, '_wp_trash_meta_status', 'publish'),
(57, 10, '_wp_trash_meta_time', '1619700867'),
(58, 10, '_wp_desired_post_slug', 'field_5ddbde5499115'),
(59, 12, '_wp_trash_meta_status', 'publish'),
(60, 12, '_wp_trash_meta_time', '1619700867'),
(61, 12, '_wp_desired_post_slug', 'field_5ddbe5aea4275'),
(62, 13, '_wp_trash_meta_status', 'publish'),
(63, 13, '_wp_trash_meta_time', '1619700867'),
(64, 13, '_wp_desired_post_slug', 'field_5ddbe636a4276'),
(65, 31, '_wp_attached_file', '2021/04/logo-e1619708710581.png'),
(66, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:862;s:6:\"height\";i:131;s:4:\"file\";s:31:\"2021/04/logo-e1619708710581.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"logo-e1619708710581-300x46.png\";s:5:\"width\";i:300;s:6:\"height\";i:46;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"logo-e1619708710581-150x131.png\";s:5:\"width\";i:150;s:6:\"height\";i:131;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"logo-e1619708710581-768x117.png\";s:5:\"width\";i:768;s:6:\"height\";i:117;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:30:\"logo-e1619708710581-134x20.png\";s:5:\"width\";i:134;s:6:\"height\";i:20;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(67, 32, '_wp_attached_file', '2021/04/logo-blue.png'),
(68, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:616;s:6:\"height\";i:75;s:4:\"file\";s:21:\"2021/04/logo-blue.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"logo-blue-300x37.png\";s:5:\"width\";i:300;s:6:\"height\";i:37;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-blue-150x75.png\";s:5:\"width\";i:150;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"logo-blue-134x16.png\";s:5:\"width\";i:134;s:6:\"height\";i:16;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(69, 33, '_wp_attached_file', '2021/04/symbol-electra.png'),
(70, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:44;s:6:\"height\";i:44;s:4:\"file\";s:26:\"2021/04/symbol-electra.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(71, 63, '_edit_last', '1'),
(72, 63, '_edit_lock', '1619780859:1'),
(73, 66, '_wp_attached_file', '2021/04/sample-video.mp4'),
(74, 66, '_wp_attachment_metadata', 'a:10:{s:8:\"filesize\";i:3114374;s:9:\"mime_type\";s:9:\"video/mp4\";s:6:\"length\";i:31;s:16:\"length_formatted\";s:4:\"0:31\";s:5:\"width\";i:640;s:6:\"height\";i:360;s:10:\"fileformat\";s:3:\"mp4\";s:10:\"dataformat\";s:9:\"quicktime\";s:5:\"audio\";a:7:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";}s:17:\"created_timestamp\";i:1438938785;}'),
(75, 67, '_wp_attached_file', '2021/04/img-1.png'),
(76, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:879;s:6:\"height\";i:562;s:4:\"file\";s:17:\"2021/04/img-1.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img-1-300x192.png\";s:5:\"width\";i:300;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"img-1-768x491.png\";s:5:\"width\";i:768;s:6:\"height\";i:491;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:16:\"img-1-134x86.png\";s:5:\"width\";i:134;s:6:\"height\";i:86;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(77, 68, '_wp_attached_file', '2021/04/img-2.png'),
(78, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:848;s:6:\"height\";i:562;s:4:\"file\";s:17:\"2021/04/img-2.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img-2-300x199.png\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"img-2-768x509.png\";s:5:\"width\";i:768;s:6:\"height\";i:509;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:16:\"img-2-134x89.png\";s:5:\"width\";i:134;s:6:\"height\";i:89;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(79, 69, '_wp_attached_file', '2021/04/img-3.png'),
(80, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:879;s:6:\"height\";i:562;s:4:\"file\";s:17:\"2021/04/img-3.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img-3-300x192.png\";s:5:\"width\";i:300;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"img-3-768x491.png\";s:5:\"width\";i:768;s:6:\"height\";i:491;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:16:\"img-3-134x86.png\";s:5:\"width\";i:134;s:6:\"height\";i:86;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(81, 70, '_wp_attached_file', '2021/04/img-4.png'),
(82, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:838;s:6:\"height\";i:562;s:4:\"file\";s:17:\"2021/04/img-4.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img-4-300x201.png\";s:5:\"width\";i:300;s:6:\"height\";i:201;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"img-4-768x515.png\";s:5:\"width\";i:768;s:6:\"height\";i:515;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:16:\"img-4-134x90.png\";s:5:\"width\";i:134;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(83, 5, 'main_video', '66'),
(84, 5, '_main_video', 'field_608aaccb4ceb2'),
(85, 5, 'main_text', 'לעלות בידיים טובות'),
(86, 5, '_main_text', 'field_608aad4103a5d'),
(87, 5, 'benefit_item_0_benefit_img', '67'),
(88, 5, '_benefit_item_0_benefit_img', 'field_608aad99197be'),
(89, 5, 'benefit_item_0_benefit_text', '<h2>מומחיות ומקצועיות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(90, 5, '_benefit_item_0_benefit_text', 'field_608aadbe197c0'),
(91, 5, 'benefit_item_1_benefit_img', '68'),
(92, 5, '_benefit_item_1_benefit_img', 'field_608aad99197be'),
(93, 5, 'benefit_item_1_benefit_text', '<h2>מגוון פתרונות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(94, 5, '_benefit_item_1_benefit_text', 'field_608aadbe197c0'),
(95, 5, 'benefit_item_2_benefit_img', '69'),
(96, 5, '_benefit_item_2_benefit_img', 'field_608aad99197be'),
(97, 5, 'benefit_item_2_benefit_text', '<h2>עיצוב וחדשנות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(98, 5, '_benefit_item_2_benefit_text', 'field_608aadbe197c0'),
(99, 5, 'benefit_item_3_benefit_img', '70'),
(100, 5, '_benefit_item_3_benefit_img', 'field_608aad99197be'),
(101, 5, 'benefit_item_3_benefit_text', '<h2>שירות ויחס אישי</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(102, 5, '_benefit_item_3_benefit_text', 'field_608aadbe197c0'),
(103, 5, 'benefit_item', '4'),
(104, 5, '_benefit_item', 'field_608aad82197bd'),
(105, 5, 'mid_form_title', 'הגיע הזמן לעלות ביידים טובות - השאירו פרטים עכשיו'),
(106, 5, '_mid_form_title', 'field_608aaded197c2'),
(107, 5, 'mid_form_text', 'הצטרפו למשפחת אלקטרה לעלול קומה בצורה בטוחה עם אחריות מלאה'),
(108, 5, '_mid_form_text', 'field_608aadfa197c3'),
(109, 5, 'counter_item_0_counter_num', '30000'),
(110, 5, '_counter_item_0_counter_num', 'field_608aae35197c6'),
(111, 5, 'counter_item_0_counter_title', 'התקנות של פתרונות'),
(112, 5, '_counter_item_0_counter_title', 'field_608aae49197c7'),
(113, 5, 'counter_item_1_counter_num', '505'),
(114, 5, '_counter_item_1_counter_num', 'field_608aae35197c6'),
(115, 5, 'counter_item_1_counter_title', 'ישובים ברחבי הארץ'),
(116, 5, '_counter_item_1_counter_title', 'field_608aae49197c7'),
(117, 5, 'counter_item_2_counter_num', '3000'),
(118, 5, '_counter_item_2_counter_num', 'field_608aae35197c6'),
(119, 5, 'counter_item_2_counter_title', 'אנשי שירות'),
(120, 5, '_counter_item_2_counter_title', 'field_608aae49197c7'),
(121, 5, 'counter_item', '3'),
(122, 5, '_counter_item', 'field_608aae1c197c5'),
(123, 5, 'text_content', '<h2><strong>אלקטרה תעמל</strong> לעלות בידים טובות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(124, 5, '_text_content', 'field_608aae8a3d64b'),
(125, 5, 'content_img', '72'),
(126, 5, '_content_img', 'field_608aaea03d64c'),
(127, 5, 'gallery_title', 'כאן תופיע גלרייה בנושא לפי דף הבית'),
(128, 5, '_gallery_title', 'field_608aaeca3d64e'),
(129, 5, 'gallery_item', '7'),
(130, 5, '_gallery_item', 'field_608aaed33d64f'),
(131, 5, 'why_block_text', '<h2>למה אלקטרה תעמל?</h2>\r\nאנחנו באלקטרה דוגלים בשירות מלא מול הלקוח'),
(132, 5, '_why_block_text', 'field_608aaf579287e'),
(133, 5, 'why_item', '6'),
(134, 5, '_why_item', 'field_608aaf6d9287f'),
(135, 71, 'title_tag', ''),
(136, 71, '_title_tag', 'field_5ddbe7577e0e7'),
(137, 71, 'single_slider_seo', ''),
(138, 71, '_single_slider_seo', 'field_5ddbde5499115'),
(139, 71, 'single_gallery', ''),
(140, 71, '_single_gallery', 'field_5ddbe5aea4275'),
(141, 71, 'single_video_slider', ''),
(142, 71, '_single_video_slider', 'field_5ddbe636a4276'),
(143, 71, 'main_video', '66'),
(144, 71, '_main_video', 'field_608aaccb4ceb2'),
(145, 71, 'main_text', 'לעלות בידיים טובות'),
(146, 71, '_main_text', 'field_608aad4103a5d'),
(147, 71, 'benefit_item_0_benefit_img', '67'),
(148, 71, '_benefit_item_0_benefit_img', 'field_608aad99197be'),
(149, 71, 'benefit_item_0_benefit_text', '<h2>מומחיות ומקצועיות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(150, 71, '_benefit_item_0_benefit_text', 'field_608aadbe197c0'),
(151, 71, 'benefit_item_1_benefit_img', '68'),
(152, 71, '_benefit_item_1_benefit_img', 'field_608aad99197be'),
(153, 71, 'benefit_item_1_benefit_text', '<h2>מגוון פתרונות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(154, 71, '_benefit_item_1_benefit_text', 'field_608aadbe197c0'),
(155, 71, 'benefit_item_2_benefit_img', '69'),
(156, 71, '_benefit_item_2_benefit_img', 'field_608aad99197be'),
(157, 71, 'benefit_item_2_benefit_text', '<h2>עיצוב וחדשנות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(158, 71, '_benefit_item_2_benefit_text', 'field_608aadbe197c0'),
(159, 71, 'benefit_item_3_benefit_img', '70'),
(160, 71, '_benefit_item_3_benefit_img', 'field_608aad99197be'),
(161, 71, 'benefit_item_3_benefit_text', '<h2>שירות ויחס אישי</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(162, 71, '_benefit_item_3_benefit_text', 'field_608aadbe197c0'),
(163, 71, 'benefit_item', '4'),
(164, 71, '_benefit_item', 'field_608aad82197bd'),
(165, 71, 'mid_form_title', 'הגיע הזמן לעלות ביידים טובות - השאירו פרטים עכשיו'),
(166, 71, '_mid_form_title', 'field_608aaded197c2'),
(167, 71, 'mid_form_text', 'הצטרפו למשפחת אלקטרה לעלול קומה בצורה בטוחה עם אחריות מלאה'),
(168, 71, '_mid_form_text', 'field_608aadfa197c3'),
(169, 71, 'counter_item_0_counter_num', '30000'),
(170, 71, '_counter_item_0_counter_num', 'field_608aae35197c6'),
(171, 71, 'counter_item_0_counter_title', 'התקנות של פתרונות'),
(172, 71, '_counter_item_0_counter_title', 'field_608aae49197c7'),
(173, 71, 'counter_item_1_counter_num', '505'),
(174, 71, '_counter_item_1_counter_num', 'field_608aae35197c6'),
(175, 71, 'counter_item_1_counter_title', 'ישובים ברחבי הארץ'),
(176, 71, '_counter_item_1_counter_title', 'field_608aae49197c7'),
(177, 71, 'counter_item_2_counter_num', '3000'),
(178, 71, '_counter_item_2_counter_num', 'field_608aae35197c6'),
(179, 71, 'counter_item_2_counter_title', 'אנשי שירות'),
(180, 71, '_counter_item_2_counter_title', 'field_608aae49197c7'),
(181, 71, 'counter_item', '3'),
(182, 71, '_counter_item', 'field_608aae1c197c5'),
(183, 71, 'text_content', ''),
(184, 71, '_text_content', 'field_608aae8a3d64b'),
(185, 71, 'content_img', ''),
(186, 71, '_content_img', 'field_608aaea03d64c'),
(187, 71, 'gallery_title', ''),
(188, 71, '_gallery_title', 'field_608aaeca3d64e'),
(189, 71, 'gallery_item', ''),
(190, 71, '_gallery_item', 'field_608aaed33d64f'),
(191, 71, 'why_block_text', ''),
(192, 71, '_why_block_text', 'field_608aaf579287e'),
(193, 71, 'why_item', ''),
(194, 71, '_why_item', 'field_608aaf6d9287f'),
(195, 72, '_wp_attached_file', '2021/04/electra-img.png'),
(196, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:782;s:6:\"height\";i:718;s:4:\"file\";s:23:\"2021/04/electra-img.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"electra-img-300x275.png\";s:5:\"width\";i:300;s:6:\"height\";i:275;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"electra-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"electra-img-768x705.png\";s:5:\"width\";i:768;s:6:\"height\";i:705;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"electra-img-102x94.png\";s:5:\"width\";i:102;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(197, 73, '_wp_attached_file', '2021/04/gallery-1.png'),
(198, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:560;s:6:\"height\";i:442;s:4:\"file\";s:21:\"2021/04/gallery-1.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-1-300x237.png\";s:5:\"width\";i:300;s:6:\"height\";i:237;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"gallery-1-119x94.png\";s:5:\"width\";i:119;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(199, 74, '_wp_attached_file', '2021/04/gallery-2.png'),
(200, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:560;s:6:\"height\";i:517;s:4:\"file\";s:21:\"2021/04/gallery-2.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-2-300x277.png\";s:5:\"width\";i:300;s:6:\"height\";i:277;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"gallery-2-102x94.png\";s:5:\"width\";i:102;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(201, 75, '_wp_attached_file', '2021/04/gallery-3.png'),
(202, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:564;s:6:\"height\";i:343;s:4:\"file\";s:21:\"2021/04/gallery-3.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-3-300x182.png\";s:5:\"width\";i:300;s:6:\"height\";i:182;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"gallery-3-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(203, 76, '_wp_attached_file', '2021/04/gallery-4.png'),
(204, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:564;s:6:\"height\";i:337;s:4:\"file\";s:21:\"2021/04/gallery-4.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-4-300x179.png\";s:5:\"width\";i:300;s:6:\"height\";i:179;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"gallery-4-134x80.png\";s:5:\"width\";i:134;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(205, 77, '_wp_attached_file', '2021/04/gallery-5.png'),
(206, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:254;s:4:\"file\";s:21:\"2021/04/gallery-5.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-5-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"gallery-5-134x60.png\";s:5:\"width\";i:134;s:6:\"height\";i:60;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(207, 78, '_wp_attached_file', '2021/04/gallery-6.png'),
(208, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:553;s:6:\"height\";i:706;s:4:\"file\";s:21:\"2021/04/gallery-6.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-6-235x300.png\";s:5:\"width\";i:235;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"gallery-6-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(209, 79, '_wp_attached_file', '2021/04/gallery-7.png'),
(210, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:553;s:6:\"height\";i:254;s:4:\"file\";s:21:\"2021/04/gallery-7.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"gallery-7-300x138.png\";s:5:\"width\";i:300;s:6:\"height\";i:138;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"gallery-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"gallery-7-134x62.png\";s:5:\"width\";i:134;s:6:\"height\";i:62;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(211, 80, '_wp_attached_file', '2021/04/why-1.png'),
(212, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:108;s:6:\"height\";i:108;s:4:\"file\";s:17:\"2021/04/why-1.png\";s:5:\"sizes\";a:1:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"why-1-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 81, '_wp_attached_file', '2021/04/why-2.png'),
(214, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:109;s:6:\"height\";i:109;s:4:\"file\";s:17:\"2021/04/why-2.png\";s:5:\"sizes\";a:1:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"why-2-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(215, 82, '_wp_attached_file', '2021/04/why-3.png'),
(216, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:97;s:6:\"height\";i:97;s:4:\"file\";s:17:\"2021/04/why-3.png\";s:5:\"sizes\";a:1:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"why-3-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 83, '_wp_attached_file', '2021/04/why-4.png'),
(218, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:115;s:6:\"height\";i:115;s:4:\"file\";s:17:\"2021/04/why-4.png\";s:5:\"sizes\";a:1:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"why-4-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(219, 84, '_wp_attached_file', '2021/04/why-5.png'),
(220, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:115;s:6:\"height\";i:115;s:4:\"file\";s:17:\"2021/04/why-5.png\";s:5:\"sizes\";a:1:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"why-5-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(221, 85, '_wp_attached_file', '2021/04/why-6.png'),
(222, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:116;s:6:\"height\";i:116;s:4:\"file\";s:17:\"2021/04/why-6.png\";s:5:\"sizes\";a:1:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"why-6-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(223, 5, 'gallery_item_0_gallery_item_img', '73'),
(224, 5, '_gallery_item_0_gallery_item_img', 'field_608aaee33d650'),
(225, 5, 'gallery_item_0_gallery_item_title', 'לורם איפסום דולור 1'),
(226, 5, '_gallery_item_0_gallery_item_title', 'field_608aaef43d651'),
(227, 5, 'gallery_item_0_gallery_item_text', 'זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(228, 5, '_gallery_item_0_gallery_item_text', 'field_608aaf163d652'),
(229, 5, 'gallery_item_1_gallery_item_img', '74'),
(230, 5, '_gallery_item_1_gallery_item_img', 'field_608aaee33d650'),
(231, 5, 'gallery_item_1_gallery_item_title', 'לורם איפסום דולור 2'),
(232, 5, '_gallery_item_1_gallery_item_title', 'field_608aaef43d651'),
(233, 5, 'gallery_item_1_gallery_item_text', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן נולום ארווס סאפיאן - פוסיליס קוויס'),
(234, 5, '_gallery_item_1_gallery_item_text', 'field_608aaf163d652'),
(235, 5, 'gallery_item_2_gallery_item_img', '75'),
(236, 5, '_gallery_item_2_gallery_item_img', 'field_608aaee33d650'),
(237, 5, 'gallery_item_2_gallery_item_title', 'מעלית יוקרה מלון דן'),
(238, 5, '_gallery_item_2_gallery_item_title', 'field_608aaef43d651'),
(239, 5, 'gallery_item_2_gallery_item_text', 'פרוייקט לרשת המלונות דן מעליות \r\nבעיצוב אישי כיאה למלון בוטיק לורם אפסם'),
(240, 5, '_gallery_item_2_gallery_item_text', 'field_608aaf163d652'),
(241, 5, 'gallery_item_3_gallery_item_img', '76'),
(242, 5, '_gallery_item_3_gallery_item_img', 'field_608aaee33d650'),
(243, 5, 'gallery_item_3_gallery_item_title', 'לורם איפסום דולור 4'),
(244, 5, '_gallery_item_3_gallery_item_title', 'field_608aaef43d651'),
(245, 5, 'gallery_item_3_gallery_item_text', 'אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן נולום ארווס סאפיאן - פוסיליס קוויס'),
(246, 5, '_gallery_item_3_gallery_item_text', 'field_608aaf163d652'),
(247, 5, 'gallery_item_4_gallery_item_img', '77'),
(248, 5, '_gallery_item_4_gallery_item_img', 'field_608aaee33d650'),
(249, 5, 'gallery_item_4_gallery_item_title', 'לורם איפסום דולור 5'),
(250, 5, '_gallery_item_4_gallery_item_title', 'field_608aaef43d651'),
(251, 5, 'gallery_item_4_gallery_item_text', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(252, 5, '_gallery_item_4_gallery_item_text', 'field_608aaf163d652'),
(253, 5, 'gallery_item_5_gallery_item_img', '78'),
(254, 5, '_gallery_item_5_gallery_item_img', 'field_608aaee33d650'),
(255, 5, 'gallery_item_5_gallery_item_title', 'לורם איפסום דולור 6'),
(256, 5, '_gallery_item_5_gallery_item_title', 'field_608aaef43d651'),
(257, 5, 'gallery_item_5_gallery_item_text', 'זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(258, 5, '_gallery_item_5_gallery_item_text', 'field_608aaf163d652'),
(259, 5, 'gallery_item_6_gallery_item_img', '79'),
(260, 5, '_gallery_item_6_gallery_item_img', 'field_608aaee33d650'),
(261, 5, 'gallery_item_6_gallery_item_title', 'לורם איפסום דולור 7'),
(262, 5, '_gallery_item_6_gallery_item_title', 'field_608aaef43d651'),
(263, 5, 'gallery_item_6_gallery_item_text', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(264, 5, '_gallery_item_6_gallery_item_text', 'field_608aaf163d652'),
(265, 5, 'why_item_0_why_icon', '80'),
(266, 5, '_why_item_0_why_icon', 'field_608aaf8492880'),
(267, 5, 'why_item_0_why_title', 'זמינות מסביב לשעון'),
(268, 5, '_why_item_0_why_title', 'field_608aaf9f92881'),
(269, 5, 'why_item_0_why_text', 'עובדים עם לורם אפסום בארץ'),
(270, 5, '_why_item_0_why_text', 'field_608aafaa92882'),
(271, 5, 'why_item_1_why_icon', '81'),
(272, 5, '_why_item_1_why_icon', 'field_608aaf8492880'),
(273, 5, 'why_item_1_why_title', 'שירות בפריסה ארצית'),
(274, 5, '_why_item_1_why_title', 'field_608aaf9f92881'),
(275, 5, 'why_item_1_why_text', 'מעל לורם אפסום שנות שירות'),
(276, 5, '_why_item_1_why_text', 'field_608aafaa92882'),
(277, 5, 'why_item_2_why_icon', '82'),
(278, 5, '_why_item_2_why_icon', 'field_608aaf8492880'),
(279, 5, 'why_item_2_why_title', ' הגב של קבוצת אלקטרה'),
(280, 5, '_why_item_2_why_title', 'field_608aaf9f92881'),
(281, 5, 'why_item_2_why_text', 'כאן תופיע כותרת לורם אפסום'),
(282, 5, '_why_item_2_why_text', 'field_608aafaa92882'),
(283, 5, 'why_item_3_why_icon', '83'),
(284, 5, '_why_item_3_why_icon', 'field_608aaf8492880'),
(285, 5, 'why_item_3_why_title', 'ליווי אישי ומנהל פרויקט אישי'),
(286, 5, '_why_item_3_why_title', 'field_608aaf9f92881'),
(287, 5, 'why_item_3_why_text', 'מנהל מכירות צמוד לכל לקוח'),
(288, 5, '_why_item_3_why_text', 'field_608aafaa92882'),
(289, 5, 'why_item_4_why_icon', '84'),
(290, 5, '_why_item_4_why_icon', 'field_608aaf8492880'),
(291, 5, 'why_item_4_why_title', 'פגישת ייעוץ בבית לקוח ללא עלות'),
(292, 5, '_why_item_4_why_title', 'field_608aaf9f92881'),
(293, 5, 'why_item_4_why_text', 'מעל 4500 לקוחות מרוצים'),
(294, 5, '_why_item_4_why_text', 'field_608aafaa92882'),
(295, 5, 'why_item_5_why_icon', '85'),
(296, 5, '_why_item_5_why_icon', 'field_608aaf8492880'),
(297, 5, 'why_item_5_why_title', 'אפשרויות מימון'),
(298, 5, '_why_item_5_why_title', 'field_608aaf9f92881'),
(299, 5, 'why_item_5_why_text', 'אספקה מהירה הכל תחת משפחה אחת'),
(300, 5, '_why_item_5_why_text', 'field_608aafaa92882'),
(301, 86, 'title_tag', ''),
(302, 86, '_title_tag', 'field_5ddbe7577e0e7'),
(303, 86, 'single_slider_seo', ''),
(304, 86, '_single_slider_seo', 'field_5ddbde5499115'),
(305, 86, 'single_gallery', ''),
(306, 86, '_single_gallery', 'field_5ddbe5aea4275'),
(307, 86, 'single_video_slider', ''),
(308, 86, '_single_video_slider', 'field_5ddbe636a4276'),
(309, 86, 'main_video', '66'),
(310, 86, '_main_video', 'field_608aaccb4ceb2'),
(311, 86, 'main_text', 'לעלות בידיים טובות'),
(312, 86, '_main_text', 'field_608aad4103a5d'),
(313, 86, 'benefit_item_0_benefit_img', '67'),
(314, 86, '_benefit_item_0_benefit_img', 'field_608aad99197be'),
(315, 86, 'benefit_item_0_benefit_text', '<h2>מומחיות ומקצועיות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(316, 86, '_benefit_item_0_benefit_text', 'field_608aadbe197c0'),
(317, 86, 'benefit_item_1_benefit_img', '68'),
(318, 86, '_benefit_item_1_benefit_img', 'field_608aad99197be'),
(319, 86, 'benefit_item_1_benefit_text', '<h2>מגוון פתרונות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(320, 86, '_benefit_item_1_benefit_text', 'field_608aadbe197c0'),
(321, 86, 'benefit_item_2_benefit_img', '69'),
(322, 86, '_benefit_item_2_benefit_img', 'field_608aad99197be'),
(323, 86, 'benefit_item_2_benefit_text', '<h2>עיצוב וחדשנות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(324, 86, '_benefit_item_2_benefit_text', 'field_608aadbe197c0'),
(325, 86, 'benefit_item_3_benefit_img', '70'),
(326, 86, '_benefit_item_3_benefit_img', 'field_608aad99197be'),
(327, 86, 'benefit_item_3_benefit_text', '<h2>שירות ויחס אישי</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(328, 86, '_benefit_item_3_benefit_text', 'field_608aadbe197c0'),
(329, 86, 'benefit_item', '4'),
(330, 86, '_benefit_item', 'field_608aad82197bd'),
(331, 86, 'mid_form_title', 'הגיע הזמן לעלות ביידים טובות - השאירו פרטים עכשיו'),
(332, 86, '_mid_form_title', 'field_608aaded197c2'),
(333, 86, 'mid_form_text', 'הצטרפו למשפחת אלקטרה לעלול קומה בצורה בטוחה עם אחריות מלאה'),
(334, 86, '_mid_form_text', 'field_608aadfa197c3'),
(335, 86, 'counter_item_0_counter_num', '30000'),
(336, 86, '_counter_item_0_counter_num', 'field_608aae35197c6'),
(337, 86, 'counter_item_0_counter_title', 'התקנות של פתרונות'),
(338, 86, '_counter_item_0_counter_title', 'field_608aae49197c7'),
(339, 86, 'counter_item_1_counter_num', '505'),
(340, 86, '_counter_item_1_counter_num', 'field_608aae35197c6'),
(341, 86, 'counter_item_1_counter_title', 'ישובים ברחבי הארץ'),
(342, 86, '_counter_item_1_counter_title', 'field_608aae49197c7'),
(343, 86, 'counter_item_2_counter_num', '3000'),
(344, 86, '_counter_item_2_counter_num', 'field_608aae35197c6'),
(345, 86, 'counter_item_2_counter_title', 'אנשי שירות'),
(346, 86, '_counter_item_2_counter_title', 'field_608aae49197c7'),
(347, 86, 'counter_item', '3'),
(348, 86, '_counter_item', 'field_608aae1c197c5'),
(349, 86, 'text_content', '<h2><strong>אלקטרה תעמל</strong> לעלות בידים טובות</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(350, 86, '_text_content', 'field_608aae8a3d64b'),
(351, 86, 'content_img', '72'),
(352, 86, '_content_img', 'field_608aaea03d64c'),
(353, 86, 'gallery_title', 'כאן תופיע גלרייה בנושא לפי דף הבית'),
(354, 86, '_gallery_title', 'field_608aaeca3d64e'),
(355, 86, 'gallery_item', '7'),
(356, 86, '_gallery_item', 'field_608aaed33d64f'),
(357, 86, 'why_block_text', '<h2>למה אלקטרה תעמל?</h2>\r\nאנחנו באלקטרה דוגלים בשירות מלא מול הלקוח'),
(358, 86, '_why_block_text', 'field_608aaf579287e'),
(359, 86, 'why_item', '6'),
(360, 86, '_why_item', 'field_608aaf6d9287f'),
(361, 86, 'gallery_item_0_gallery_item_img', '73'),
(362, 86, '_gallery_item_0_gallery_item_img', 'field_608aaee33d650'),
(363, 86, 'gallery_item_0_gallery_item_title', 'לורם איפסום דולור 1'),
(364, 86, '_gallery_item_0_gallery_item_title', 'field_608aaef43d651'),
(365, 86, 'gallery_item_0_gallery_item_text', 'זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(366, 86, '_gallery_item_0_gallery_item_text', 'field_608aaf163d652'),
(367, 86, 'gallery_item_1_gallery_item_img', '74'),
(368, 86, '_gallery_item_1_gallery_item_img', 'field_608aaee33d650'),
(369, 86, 'gallery_item_1_gallery_item_title', 'לורם איפסום דולור 2'),
(370, 86, '_gallery_item_1_gallery_item_title', 'field_608aaef43d651'),
(371, 86, 'gallery_item_1_gallery_item_text', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן נולום ארווס סאפיאן - פוסיליס קוויס'),
(372, 86, '_gallery_item_1_gallery_item_text', 'field_608aaf163d652'),
(373, 86, 'gallery_item_2_gallery_item_img', '75'),
(374, 86, '_gallery_item_2_gallery_item_img', 'field_608aaee33d650'),
(375, 86, 'gallery_item_2_gallery_item_title', 'מעלית יוקרה מלון דן'),
(376, 86, '_gallery_item_2_gallery_item_title', 'field_608aaef43d651'),
(377, 86, 'gallery_item_2_gallery_item_text', 'פרוייקט לרשת המלונות דן מעליות \r\nבעיצוב אישי כיאה למלון בוטיק לורם אפסם'),
(378, 86, '_gallery_item_2_gallery_item_text', 'field_608aaf163d652'),
(379, 86, 'gallery_item_3_gallery_item_img', '76'),
(380, 86, '_gallery_item_3_gallery_item_img', 'field_608aaee33d650'),
(381, 86, 'gallery_item_3_gallery_item_title', 'לורם איפסום דולור 4'),
(382, 86, '_gallery_item_3_gallery_item_title', 'field_608aaef43d651'),
(383, 86, 'gallery_item_3_gallery_item_text', 'אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן נולום ארווס סאפיאן - פוסיליס קוויס'),
(384, 86, '_gallery_item_3_gallery_item_text', 'field_608aaf163d652'),
(385, 86, 'gallery_item_4_gallery_item_img', '77'),
(386, 86, '_gallery_item_4_gallery_item_img', 'field_608aaee33d650'),
(387, 86, 'gallery_item_4_gallery_item_title', 'לורם איפסום דולור 5'),
(388, 86, '_gallery_item_4_gallery_item_title', 'field_608aaef43d651'),
(389, 86, 'gallery_item_4_gallery_item_text', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(390, 86, '_gallery_item_4_gallery_item_text', 'field_608aaf163d652'),
(391, 86, 'gallery_item_5_gallery_item_img', '78'),
(392, 86, '_gallery_item_5_gallery_item_img', 'field_608aaee33d650'),
(393, 86, 'gallery_item_5_gallery_item_title', 'לורם איפסום דולור 6'),
(394, 86, '_gallery_item_5_gallery_item_title', 'field_608aaef43d651'),
(395, 86, 'gallery_item_5_gallery_item_text', 'זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(396, 86, '_gallery_item_5_gallery_item_text', 'field_608aaf163d652'),
(397, 86, 'gallery_item_6_gallery_item_img', '79'),
(398, 86, '_gallery_item_6_gallery_item_img', 'field_608aaee33d650'),
(399, 86, 'gallery_item_6_gallery_item_title', 'לורם איפסום דולור 7'),
(400, 86, '_gallery_item_6_gallery_item_title', 'field_608aaef43d651'),
(401, 86, 'gallery_item_6_gallery_item_text', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.'),
(402, 86, '_gallery_item_6_gallery_item_text', 'field_608aaf163d652'),
(403, 86, 'why_item_0_why_icon', '80'),
(404, 86, '_why_item_0_why_icon', 'field_608aaf8492880'),
(405, 86, 'why_item_0_why_title', 'זמינות מסביב לשעון'),
(406, 86, '_why_item_0_why_title', 'field_608aaf9f92881'),
(407, 86, 'why_item_0_why_text', 'עובדים עם לורם אפסום בארץ'),
(408, 86, '_why_item_0_why_text', 'field_608aafaa92882'),
(409, 86, 'why_item_1_why_icon', '81'),
(410, 86, '_why_item_1_why_icon', 'field_608aaf8492880'),
(411, 86, 'why_item_1_why_title', 'שירות בפריסה ארצית'),
(412, 86, '_why_item_1_why_title', 'field_608aaf9f92881'),
(413, 86, 'why_item_1_why_text', 'מעל לורם אפסום שנות שירות'),
(414, 86, '_why_item_1_why_text', 'field_608aafaa92882'),
(415, 86, 'why_item_2_why_icon', '82'),
(416, 86, '_why_item_2_why_icon', 'field_608aaf8492880'),
(417, 86, 'why_item_2_why_title', ' הגב של קבוצת אלקטרה'),
(418, 86, '_why_item_2_why_title', 'field_608aaf9f92881'),
(419, 86, 'why_item_2_why_text', 'כאן תופיע כותרת לורם אפסום'),
(420, 86, '_why_item_2_why_text', 'field_608aafaa92882'),
(421, 86, 'why_item_3_why_icon', '83'),
(422, 86, '_why_item_3_why_icon', 'field_608aaf8492880'),
(423, 86, 'why_item_3_why_title', 'ליווי אישי ומנהל פרויקט אישי'),
(424, 86, '_why_item_3_why_title', 'field_608aaf9f92881'),
(425, 86, 'why_item_3_why_text', 'מנהל מכירות צמוד לכל לקוח'),
(426, 86, '_why_item_3_why_text', 'field_608aafaa92882'),
(427, 86, 'why_item_4_why_icon', '84'),
(428, 86, '_why_item_4_why_icon', 'field_608aaf8492880'),
(429, 86, 'why_item_4_why_title', 'פגישת ייעוץ בבית לקוח ללא עלות'),
(430, 86, '_why_item_4_why_title', 'field_608aaf9f92881'),
(431, 86, 'why_item_4_why_text', 'מעל 4500 לקוחות מרוצים'),
(432, 86, '_why_item_4_why_text', 'field_608aafaa92882'),
(433, 86, 'why_item_5_why_icon', '85'),
(434, 86, '_why_item_5_why_icon', 'field_608aaf8492880'),
(435, 86, 'why_item_5_why_title', 'אפשרויות מימון'),
(436, 86, '_why_item_5_why_title', 'field_608aaf9f92881'),
(437, 86, 'why_item_5_why_text', 'אספקה מהירה הכל תחת משפחה אחת'),
(438, 86, '_why_item_5_why_text', 'field_608aafaa92882'),
(439, 88, '_wp_attached_file', '2021/04/logo-text.png'),
(440, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:639;s:6:\"height\";i:107;s:4:\"file\";s:21:\"2021/04/logo-text.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"logo-text-300x50.png\";s:5:\"width\";i:300;s:6:\"height\";i:50;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"logo-text-150x107.png\";s:5:\"width\";i:150;s:6:\"height\";i:107;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"logo-text-134x22.png\";s:5:\"width\";i:134;s:6:\"height\";i:22;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(441, 31, '_wp_attachment_backup_sizes', 'a:5:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:862;s:6:\"height\";i:159;s:4:\"file\";s:8:\"logo.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:16:\"logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:15:\"logo-300x55.png\";s:5:\"width\";i:300;s:6:\"height\";i:55;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"medium_large-orig\";a:4:{s:4:\"file\";s:16:\"logo-768x142.png\";s:5:\"width\";i:768;s:6:\"height\";i:142;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:15:\"logo-134x25.png\";s:5:\"width\";i:134;s:6:\"height\";i:25;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(442, 89, '_edit_last', '1'),
(443, 89, '_edit_lock', '1619780861:1'),
(444, 30, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_posts`
--

CREATE TABLE `fmn_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_posts`
--

INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-04-29 12:58:11', '2021-04-29 09:58:11', '<!-- wp:paragraph -->\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.</p>\n<!-- /wp:paragraph -->', 'שלום עולם!', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d', '', '', '2021-04-29 12:58:11', '2021-04-29 09:58:11', '', 0, 'http://electra:8888/?p=1', 0, 'post', '', 1),
(2, 1, '2021-04-29 12:58:11', '2021-04-29 09:58:11', '<!-- wp:paragraph -->\n<p>זהו עמוד לדוגמה. הוא שונה מפוסט רגיל בבלוג מכיוון שהוא יישאר במקום אחד ויופיע בתפריט הניווט באתר (ברוב התבניות). רוב האנשים מתחילים עם עמוד אודות המציג אותם למבקרים חדשים באתר. הנה תוכן לדוגמה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>אהלן! אני סטודנט לכלכלה ביום, אך מבלה את הלילות בהגשמת החלום שלי להיות שחקן תאטרון. אני גר ביפו, יש לי כלב נהדר בשם שוקו, אני אוהב ערק אשכוליות בשישי בצהריים (במיוחד תוך כדי משחק שש-בש על חוף הים). ברוכים הבאים לאתר שלי!</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>... או משהו כזה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>החברה א.א. שומדברים נוסדה בשנת 1971, והיא מספקת שומדברים איכותיים לציבור מאז. א.א. שומדברים ממוקמת בעיר תקוות-ים, מעסיקה מעל 2,000 אנשים ועושה כל מיני דברים מדהים עבור הקהילה התקוות-ימית.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>כמשתמש וורדפרס חדש, כדאי לבקר <a href=\"http://electra:8888/wp-admin/\">בלוח הבקרה שלך</a> כדי למחוק את העמוד הזה, וליצור עמודים חדשים עבור התוכן שלך. שיהיה בכיף!</p>\n<!-- /wp:paragraph -->', 'עמוד לדוגמא', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-04-29 12:58:11', '2021-04-29 09:58:11', '', 0, 'http://electra:8888/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-04-29 12:58:11', '2021-04-29 09:58:11', '<!-- wp:heading --><h2>מי אנחנו</h2><!-- /wp:heading --><!-- wp:paragraph --><p>כתובת האתר שלנו היא: http://electra:8888.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>מה המידע האישי שאנו אוספים ומדוע אנחנו אוספים אותו</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>תגובות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כאשר המבקרים משאירים תגובות באתר אנו אוספים את הנתונים המוצגים בטופס התגובה, ובנוסף גם את כתובת ה-IP של המבקר, ואת מחרוזת ה-user agent של הדפדפן שלו כדי לסייע בזיהוי תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>יתכן ונעביר מחרוזת אנונימית שנוצרה מכתובת הדואר האלקטרוני שלך (הנקראת גם hash) לשירות Gravatar כדי לראות אם הנך חבר/ה בשירות. מדיניות הפרטיות של שירות Gravatar זמינה כאן: https://automattic.com/privacy/. לאחר אישור התגובה שלך, תמונת הפרופיל שלך גלויה לציבור בהקשר של התגובה שלך.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>מדיה</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בהעלאה של תמונות לאתר, מומלץ להימנע מהעלאת תמונות עם נתוני מיקום מוטבעים (EXIF GPS). המבקרים באתר יכולים להוריד ולחלץ את כל נתוני מיקום מהתמונות באתר.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>טפסי יצירת קשר</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>עוגיות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בכתיבת תגובה באתר שלנו, באפשרותך להחליט אם לאפשר לנו לשמור את השם שלך, כתובת האימייל שלך וכתובת האתר שלך בקבצי עוגיות (cookies). השמירה תתבצע לנוחיותך, על מנת שלא יהיה צורך למלא את הפרטים שלך שוב בכתיבת תגובה נוספת. קבצי העוגיות ישמרו לשנה.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה מבקר בעמוד ההתחברות של האתר, נגדיר קובץ עוגיה זמני על מנת לקבוע האם הדפדפן שלך מקבל קבצי עוגיות. קובץ עוגיה זה אינו מכיל נתונים אישיים והוא נמחק בעת סגירת הדפדפן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>כאשר תתחבר, אנחנו גם נגדיר מספר \'עוגיות\' על מנת לשמור את פרטי ההתחברות שלך ואת בחירות התצוגה שלך. עוגיות התחברות תקפות ליומיים, ועוגיות אפשרויות מסך תקפות לשנה. אם תבחר באפשרות &quot;זכור אותי&quot;, פרטי ההתחברות שלך יהיו תקפים למשך שבועיים. אם תתנתק מהחשבון שלך, עוגיות ההתחברות יימחקו.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה עורך או מפרסם מאמר, קובץ \'עוגיה\' נוסף יישמר בדפדפן שלך. קובץ \'עוגיה\' זה אינו כולל נתונים אישיים ופשוט מציין את מזהה הפוסט של המאמר. הוא יפוג לאחר יום אחד.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>תוכן מוטמע מאתרים אחרים</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כתבות או פוסטים באתר זה עשויים לכלול תוכן מוטבע (לדוגמה, קטעי וידאו, תמונות, מאמרים, וכו\'). תוכן מוטבע מאתרי אינטרנט אחרים דינו כביקור הקורא באתרי האינטרנט מהם מוטבע התוכן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אתרים אלו עשויים לאסוף נתונים אודותיך, להשתמש בקבצי \'עוגיות\', להטמיע מעקב של צד שלישי נוסף, ולנטר את האינטראקציה שלך עם תוכן מוטמע זה, לרבות מעקב אחר האינטראקציה שלך עם התוכן המוטמע, אם יש לך חשבון ואתה מחובר לאתר זה.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>אנליטיקה</h3><!-- /wp:heading --><!-- wp:heading --><h2>עם מי אנו חולקים את המידע שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>משך הזמן בו נשמור את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>במידה ותגיב/י על תוכן באתר, התגובה והנתונים אודותיה יישמרו ללא הגבלת זמן, כדי שנוכל לזהות ולאשר את כל התגובות העוקבות באופן אוטומטי.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>עבור משתמשים רשומים באתר (במידה ויש כאלה) אנו מאחסנים גם את המידע האישי שהם מספקים בפרופיל המשתמש שלהם. כל המשתמשים יכולים לראות, לערוך או למחוק את המידע האישי שלהם בכל עת (פרט לשם המשתמש אותו לא ניתן לשנות). גם מנהלי האתר יכולים לראות ולערוך מידע זה.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>אילו זכויות יש לך על המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>אם יש לך חשבון באתר זה, או שהשארת תגובות באתר, באפשרותך לבקש לקבל קובץ של הנתונים האישיים שאנו מחזיקים לגביך, כולל כל הנתונים שסיפקת לנו. באפשרותך גם לבקש שנמחק כל מידע אישי שאנו מחזיקים לגביך. הדבר אינו כולל נתונים שאנו מחויבים לשמור למטרות מנהליות, משפטיות או ביטחוניות.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>להיכן אנו שולחים את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>תגובות מבקרים עלולות להיבדק על ידי שירות אוטומטי למניעת תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>פרטי ההתקשרות שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>מידע נוסף</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>כיצד אנו מגינים על המידע שלך</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>אילו הליכי פרצת נתונים יש לנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>מאילו גופי צד-שלישי אנו מקבלים מידע</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>קבלת החלטות אוטומטיות ו/או יצירת פרופילים שאנו עושים עם נתוני המשתמשים שלנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>דרישות גילוי רגולטוריות של התעשייה </h3><!-- /wp:heading -->', 'מדיניות פרטיות', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-04-29 12:58:11', '2021-04-29 09:58:11', '', 0, 'http://electra:8888/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-04-29 12:58:21', '0000-00-00 00:00:00', '', 'טיוטה משמירה אוטומטית', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-04-29 12:58:21', '0000-00-00 00:00:00', '', 0, 'http://electra:8888/?p=4', 0, 'post', '', 0),
(5, 1, '2021-04-29 13:14:06', '2021-04-29 10:14:06', '', 'דף הבית', '', 'publish', 'closed', 'closed', '', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', '', '', '2021-04-29 22:11:19', '2021-04-29 19:11:19', '', 0, 'http://electra:8888/?page_id=5', 0, 'page', '', 0),
(6, 1, '2021-04-29 13:14:06', '2021-04-29 10:14:06', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2021-04-29 13:14:06', '2021-04-29 10:14:06', '', 5, 'http://electra:8888/?p=6', 0, 'revision', '', 0),
(8, 1, '2021-04-29 13:15:13', '2021-04-29 10:15:13', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'דף הבית', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', 'publish', 'closed', 'closed', '', 'group_608a872620c67', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 0, 'http://electra:8888/?post_type=acf-field-group&#038;p=8', 0, 'acf-field-group', '', 0),
(9, 1, '2021-04-29 13:27:57', '2021-04-29 10:27:57', 'a:9:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:8:\"modified\";i:1574692511;s:5:\"local\";s:4:\"json\";}', 'תוספות תוכן', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa-%d7%aa%d7%95%d7%9b%d7%9f', 'trash', 'closed', 'closed', '', 'group_5ddbde4d59412__trashed', '', '', '2021-04-29 15:54:27', '2021-04-29 12:54:27', '', 0, 'http://electra:8888/?p=9', 0, 'acf-field-group', '', 0),
(10, 1, '2021-04-29 13:27:57', '2021-04-29 10:27:57', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר SEO', 'single_slider_seo', 'trash', 'closed', 'closed', '', 'field_5ddbde5499115__trashed', '', '', '2021-04-29 15:54:27', '2021-04-29 12:54:27', '', 9, 'http://electra:8888/?post_type=acf-field&#038;p=10', 0, 'acf-field', '', 0),
(11, 1, '2021-04-29 13:27:57', '2021-04-29 10:27:57', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_5ddbde7399116', '', '', '2021-04-29 13:27:57', '2021-04-29 10:27:57', '', 10, 'http://electra:8888/?post_type=acf-field&p=11', 0, 'acf-field', '', 0),
(12, 1, '2021-04-29 13:27:57', '2021-04-29 10:27:57', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'גלריה תמונות', 'single_gallery', 'trash', 'closed', 'closed', '', 'field_5ddbe5aea4275__trashed', '', '', '2021-04-29 15:54:27', '2021-04-29 12:54:27', '', 9, 'http://electra:8888/?post_type=acf-field&#038;p=12', 1, 'acf-field', '', 0),
(13, 1, '2021-04-29 13:27:57', '2021-04-29 10:27:57', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר סרטונים', 'single_video_slider', 'trash', 'closed', 'closed', '', 'field_5ddbe636a4276__trashed', '', '', '2021-04-29 15:54:27', '2021-04-29 12:54:27', '', 9, 'http://electra:8888/?post_type=acf-field&#038;p=13', 2, 'acf-field', '', 0),
(14, 1, '2021-04-29 13:27:57', '2021-04-29 10:27:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'URL', 'url', 'publish', 'closed', 'closed', '', 'field_5ddbe693a4277', '', '', '2021-04-29 13:27:57', '2021-04-29 10:27:57', '', 13, 'http://electra:8888/?post_type=acf-field&p=14', 0, 'acf-field', '', 0),
(16, 1, '2021-04-29 13:28:03', '2021-04-29 10:28:03', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו', 'logo', 'draft', 'closed', 'closed', '', 'field_5dd3a310760b9', '', '', '2021-04-29 13:28:26', '2021-04-29 10:28:26', '', 15, 'http://electra:8888/?post_type=acf-field&#038;p=16', 0, 'acf-field', '', 0),
(17, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:9:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:8:\"modified\";i:1574688481;s:5:\"local\";s:4:\"json\";}', 'פרטי קשר', '%d7%a4%d7%a8%d7%98%d7%99-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_5dd3a32b38e68', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 0, 'http://electra:8888/?p=17', 0, 'acf-field-group', '', 0),
(18, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'טלפון', 'tel', 'publish', 'closed', 'closed', '', 'field_5dd3a33f42e0c', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=18', 0, 'acf-field', '', 0),
(19, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מייל', 'mail', 'publish', 'closed', 'closed', '', 'field_5dd3a35642e0d', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=19', 1, 'acf-field', '', 0),
(20, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'פקס', 'fax', 'publish', 'closed', 'closed', '', 'field_5dd3a36942e0e', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=20', 2, 'acf-field', '', 0),
(21, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כתובת', 'address', 'publish', 'closed', 'closed', '', 'field_5dd3a4d142e0f', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=21', 3, 'acf-field', '', 0),
(22, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'מפה (תמונה)', 'map_image', 'publish', 'closed', 'closed', '', 'field_5ddbd67734310', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=22', 4, 'acf-field', '', 0),
(23, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'פייסבוק', 'facebook', 'publish', 'closed', 'closed', '', 'field_5ddbd6b3cc0cd', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=23', 5, 'acf-field', '', 0),
(24, 1, '2021-04-29 13:28:07', '2021-04-29 10:28:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Whatsapp', 'whatsapp', 'publish', 'closed', 'closed', '', 'field_5ddbd6cdcc0ce', '', '', '2021-04-29 13:28:07', '2021-04-29 10:28:07', '', 17, 'http://electra:8888/?post_type=acf-field&p=24', 6, 'acf-field', '', 0),
(26, 1, '2021-04-29 13:31:21', '2021-04-29 10:31:21', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תמונות', '%d7%aa%d7%9e%d7%95%d7%a0%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_608a8a89c1e57', '', '', '2021-04-29 17:57:22', '2021-04-29 14:57:22', '', 0, 'http://electra:8888/?post_type=acf-field-group&#038;p=26', 2, 'acf-field-group', '', 0),
(27, 1, '2021-04-29 13:31:21', '2021-04-29 10:31:21', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו לבן', 'logo_white', 'publish', 'closed', 'closed', '', 'field_608a8a969ed99', '', '', '2021-04-29 17:57:22', '2021-04-29 14:57:22', '', 26, 'http://electra:8888/?post_type=acf-field&#038;p=27', 0, 'acf-field', '', 0),
(28, 1, '2021-04-29 13:31:21', '2021-04-29 10:31:21', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו כחול', 'logo_blue', 'publish', 'closed', 'closed', '', 'field_608a8ab99ed9a', '', '', '2021-04-29 17:57:22', '2021-04-29 14:57:22', '', 26, 'http://electra:8888/?post_type=acf-field&#038;p=28', 1, 'acf-field', '', 0),
(29, 1, '2021-04-29 13:31:21', '2021-04-29 10:31:21', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'סמל', 'symbol', 'publish', 'closed', 'closed', '', 'field_608a8ae69ed9b', '', '', '2021-04-29 17:57:22', '2021-04-29 14:57:22', '', 26, 'http://electra:8888/?post_type=acf-field&#038;p=29', 2, 'acf-field', '', 0),
(30, 1, '2021-04-29 13:44:02', '2021-04-29 10:44:02', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-xl-3 col-md-6 col-12\">\r\n		[text* text-173 placeholder \"שם:\"]\r\n	</div>\r\n    <div class=\"col-xl-3 col-md-6 col-12\">\r\n			[tel* tel-48 placeholder \"טלפון:\"]\r\n	</div>\r\n	 <div class=\"col-xl-6 col-12\">\r\n		 [select menu-648 \"אני מעוניין שיחזרו אלי לגביי הפנייה הבאה\"]\r\n	</div>\r\n    <div class=\"col-12 submit-col\">\r\n			[submit \"אני רוצה הצעת מחיר מאלקטרה  עכשיו\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@electra>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@electra>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'טופס בסיסי', '', 'publish', 'closed', 'closed', '', '%d7%98%d7%95%d7%a4%d7%a1-%d7%91%d7%a1%d7%99%d7%a1%d7%99', '', '', '2021-04-30 14:22:40', '2021-04-30 11:22:40', '', 0, 'http://electra:8888/?post_type=wpcf7_contact_form&#038;p=30', 0, 'wpcf7_contact_form', '', 0),
(31, 1, '2021-04-29 15:54:51', '2021-04-29 12:54:51', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-04-29 15:54:51', '2021-04-29 12:54:51', '', 0, 'http://electra:8888/wp-content/uploads/2021/04/logo.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2021-04-29 15:55:01', '2021-04-29 12:55:01', '', 'logo-blue', '', 'inherit', 'open', 'closed', '', 'logo-blue', '', '', '2021-04-29 15:55:01', '2021-04-29 12:55:01', '', 0, 'http://electra:8888/wp-content/uploads/2021/04/logo-blue.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2021-04-29 15:55:28', '2021-04-29 12:55:28', '', 'symbol-electra', '', 'inherit', 'open', 'closed', '', 'symbol-electra', '', '', '2021-04-29 15:55:28', '2021-04-29 12:55:28', '', 0, 'http://electra:8888/wp-content/uploads/2021/04/symbol-electra.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2021-04-29 15:57:24', '2021-04-29 12:57:24', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:8:\"mp4, ogg\";}', 'וידאו', 'main_video', 'publish', 'closed', 'closed', '', 'field_608aaccb4ceb2', '', '', '2021-04-29 15:58:05', '2021-04-29 12:58:05', '', 8, 'http://electra:8888/?post_type=acf-field&#038;p=34', 1, 'acf-field', '', 0),
(35, 1, '2021-04-29 15:57:52', '2021-04-29 12:57:52', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:4:\"rows\";s:0:\"\";}', 'טקסט', 'main_text', 'publish', 'closed', 'closed', '', 'field_608aad4103a5d', '', '', '2021-04-29 15:58:05', '2021-04-29 12:58:05', '', 8, 'http://electra:8888/?post_type=acf-field&#038;p=35', 2, 'acf-field', '', 0),
(36, 1, '2021-04-29 15:58:05', '2021-04-29 12:58:05', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק ראשי', 'בלוק_ראשי', 'publish', 'closed', 'closed', '', 'field_608aad52845f6', '', '', '2021-04-29 15:58:05', '2021-04-29 12:58:05', '', 8, 'http://electra:8888/?post_type=acf-field&p=36', 0, 'acf-field', '', 0),
(37, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'יתרונות', 'יתרונות', 'publish', 'closed', 'closed', '', 'field_608aad5f197bc', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=37', 3, 'acf-field', '', 0),
(38, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'יתרון', 'benefit_item', 'publish', 'closed', 'closed', '', 'field_608aad82197bd', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=38', 4, 'acf-field', '', 0),
(39, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'benefit_img', 'publish', 'closed', 'closed', '', 'field_608aad99197be', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 38, 'http://electra:8888/?post_type=acf-field&p=39', 0, 'acf-field', '', 0),
(40, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'benefit_text', 'publish', 'closed', 'closed', '', 'field_608aadbe197c0', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 38, 'http://electra:8888/?post_type=acf-field&p=40', 1, 'acf-field', '', 0),
(41, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'טופס', 'טופס', 'publish', 'closed', 'closed', '', 'field_608aaddc197c1', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=41', 5, 'acf-field', '', 0),
(42, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'mid_form_title', 'publish', 'closed', 'closed', '', 'field_608aaded197c2', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=42', 6, 'acf-field', '', 0),
(43, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'mid_form_text', 'publish', 'closed', 'closed', '', 'field_608aadfa197c3', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=43', 7, 'acf-field', '', 0),
(44, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'קאונטר', 'קאונטר', 'publish', 'closed', 'closed', '', 'field_608aae0d197c4', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=44', 8, 'acf-field', '', 0),
(45, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'פריט', 'counter_item', 'publish', 'closed', 'closed', '', 'field_608aae1c197c5', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 8, 'http://electra:8888/?post_type=acf-field&p=45', 9, 'acf-field', '', 0),
(46, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מספר', 'counter_num', 'publish', 'closed', 'closed', '', 'field_608aae35197c6', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 45, 'http://electra:8888/?post_type=acf-field&p=46', 0, 'acf-field', '', 0),
(47, 1, '2021-04-29 16:02:45', '2021-04-29 13:02:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'תיאור', 'counter_title', 'publish', 'closed', 'closed', '', 'field_608aae49197c7', '', '', '2021-04-29 16:02:45', '2021-04-29 13:02:45', '', 45, 'http://electra:8888/?post_type=acf-field&p=47', 1, 'acf-field', '', 0),
(48, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם טקסט', 'בלוק_עם_טקסט', 'publish', 'closed', 'closed', '', 'field_608aae7d3d64a', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 8, 'http://electra:8888/?post_type=acf-field&p=48', 10, 'acf-field', '', 0),
(49, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'text_content', 'publish', 'closed', 'closed', '', 'field_608aae8a3d64b', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 8, 'http://electra:8888/?post_type=acf-field&p=49', 11, 'acf-field', '', 0),
(50, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'content_img', 'publish', 'closed', 'closed', '', 'field_608aaea03d64c', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 8, 'http://electra:8888/?post_type=acf-field&p=50', 12, 'acf-field', '', 0),
(51, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'גלריה', 'גלריה', 'publish', 'closed', 'closed', '', 'field_608aaeb23d64d', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 8, 'http://electra:8888/?post_type=acf-field&p=51', 13, 'acf-field', '', 0),
(52, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'gallery_title', 'publish', 'closed', 'closed', '', 'field_608aaeca3d64e', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 8, 'http://electra:8888/?post_type=acf-field&p=52', 14, 'acf-field', '', 0),
(53, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'גלריה', 'gallery_item', 'publish', 'closed', 'closed', '', 'field_608aaed33d64f', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 8, 'http://electra:8888/?post_type=acf-field&p=53', 15, 'acf-field', '', 0),
(54, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'gallery_item_img', 'publish', 'closed', 'closed', '', 'field_608aaee33d650', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 53, 'http://electra:8888/?post_type=acf-field&p=54', 0, 'acf-field', '', 0),
(55, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'gallery_item_title', 'publish', 'closed', 'closed', '', 'field_608aaef43d651', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 53, 'http://electra:8888/?post_type=acf-field&p=55', 1, 'acf-field', '', 0),
(56, 1, '2021-04-29 16:05:40', '2021-04-29 13:05:40', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'gallery_item_text', 'publish', 'closed', 'closed', '', 'field_608aaf163d652', '', '', '2021-04-29 16:05:40', '2021-04-29 13:05:40', '', 53, 'http://electra:8888/?post_type=acf-field&p=56', 2, 'acf-field', '', 0),
(57, 1, '2021-04-29 16:08:24', '2021-04-29 13:08:24', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'למה אלקטרה?', 'למה_אלקטרה', 'publish', 'closed', 'closed', '', 'field_608aaf359287c', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 8, 'http://electra:8888/?post_type=acf-field&p=57', 16, 'acf-field', '', 0),
(58, 1, '2021-04-29 16:08:24', '2021-04-29 13:08:24', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'why_block_text', 'publish', 'closed', 'closed', '', 'field_608aaf579287e', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 8, 'http://electra:8888/?post_type=acf-field&p=58', 17, 'acf-field', '', 0),
(59, 1, '2021-04-29 16:08:24', '2021-04-29 13:08:24', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'יתרון', 'why_item', 'publish', 'closed', 'closed', '', 'field_608aaf6d9287f', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 8, 'http://electra:8888/?post_type=acf-field&p=59', 18, 'acf-field', '', 0),
(60, 1, '2021-04-29 16:08:24', '2021-04-29 13:08:24', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'אייקון', 'why_icon', 'publish', 'closed', 'closed', '', 'field_608aaf8492880', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 59, 'http://electra:8888/?post_type=acf-field&p=60', 0, 'acf-field', '', 0),
(61, 1, '2021-04-29 16:08:24', '2021-04-29 13:08:24', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'why_title', 'publish', 'closed', 'closed', '', 'field_608aaf9f92881', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 59, 'http://electra:8888/?post_type=acf-field&p=61', 1, 'acf-field', '', 0),
(62, 1, '2021-04-29 16:08:24', '2021-04-29 13:08:24', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'תיאור', 'why_text', 'publish', 'closed', 'closed', '', 'field_608aafaa92882', '', '', '2021-04-29 16:08:24', '2021-04-29 13:08:24', '', 59, 'http://electra:8888/?post_type=acf-field&p=62', 2, 'acf-field', '', 0),
(63, 1, '2021-04-29 16:09:16', '2021-04-29 13:09:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'טופס בפוטר', '%d7%98%d7%95%d7%a4%d7%a1-%d7%91%d7%a4%d7%95%d7%98%d7%a8', 'publish', 'closed', 'closed', '', 'group_608aafcdad5dc', '', '', '2021-04-29 16:09:16', '2021-04-29 13:09:16', '', 0, 'http://electra:8888/?post_type=acf-field-group&#038;p=63', 20, 'acf-field-group', '', 0),
(64, 1, '2021-04-29 16:09:16', '2021-04-29 13:09:16', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:4:\"rows\";s:0:\"\";}', 'טקסט', 'foo_form_text', 'publish', 'closed', 'closed', '', 'field_608aafde239d1', '', '', '2021-04-29 16:09:16', '2021-04-29 13:09:16', '', 63, 'http://electra:8888/?post_type=acf-field&p=64', 0, 'acf-field', '', 0),
(65, 1, '2021-04-29 16:09:42', '0000-00-00 00:00:00', '', 'טיוטה אוטומטית', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-04-29 16:09:42', '0000-00-00 00:00:00', '', 0, 'http://electra:8888/?post_type=acf-field-group&p=65', 0, 'acf-field-group', '', 0),
(66, 1, '2021-04-29 16:22:44', '2021-04-29 13:22:44', '', 'sample-video', '', 'inherit', 'open', 'closed', '', 'sample-video', '', '', '2021-04-29 16:22:44', '2021-04-29 13:22:44', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/sample-video.mp4', 0, 'attachment', 'video/mp4', 0),
(67, 1, '2021-04-29 16:23:07', '2021-04-29 13:23:07', '', 'img-1', '', 'inherit', 'open', 'closed', '', 'img-1', '', '', '2021-04-29 16:23:07', '2021-04-29 13:23:07', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/img-1.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2021-04-29 16:23:08', '2021-04-29 13:23:08', '', 'img-2', '', 'inherit', 'open', 'closed', '', 'img-2', '', '', '2021-04-29 16:23:08', '2021-04-29 13:23:08', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/img-2.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2021-04-29 16:23:09', '2021-04-29 13:23:09', '', 'img-3', '', 'inherit', 'open', 'closed', '', 'img-3', '', '', '2021-04-29 16:23:09', '2021-04-29 13:23:09', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/img-3.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2021-04-29 16:23:11', '2021-04-29 13:23:11', '', 'img-4', '', 'inherit', 'open', 'closed', '', 'img-4', '', '', '2021-04-29 16:23:11', '2021-04-29 13:23:11', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/img-4.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2021-04-29 16:25:36', '2021-04-29 13:25:36', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2021-04-29 16:25:36', '2021-04-29 13:25:36', '', 5, 'http://electra:8888/?p=71', 0, 'revision', '', 0),
(72, 1, '2021-04-29 16:27:18', '2021-04-29 13:27:18', '', 'electra-img', '', 'inherit', 'open', 'closed', '', 'electra-img', '', '', '2021-04-29 16:27:18', '2021-04-29 13:27:18', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/electra-img.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2021-04-29 16:29:30', '2021-04-29 13:29:30', '', 'gallery-1', '', 'inherit', 'open', 'closed', '', 'gallery-1', '', '', '2021-04-29 16:29:30', '2021-04-29 13:29:30', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-1.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2021-04-29 16:29:30', '2021-04-29 13:29:30', '', 'gallery-2', '', 'inherit', 'open', 'closed', '', 'gallery-2', '', '', '2021-04-29 16:29:30', '2021-04-29 13:29:30', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-2.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2021-04-29 16:29:31', '2021-04-29 13:29:31', '', 'gallery-3', '', 'inherit', 'open', 'closed', '', 'gallery-3', '', '', '2021-04-29 16:29:31', '2021-04-29 13:29:31', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-3.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2021-04-29 16:29:32', '2021-04-29 13:29:32', '', 'gallery-4', '', 'inherit', 'open', 'closed', '', 'gallery-4', '', '', '2021-04-29 16:29:32', '2021-04-29 13:29:32', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-4.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2021-04-29 16:29:32', '2021-04-29 13:29:32', '', 'gallery-5', '', 'inherit', 'open', 'closed', '', 'gallery-5', '', '', '2021-04-29 16:29:32', '2021-04-29 13:29:32', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-5.png', 0, 'attachment', 'image/png', 0),
(78, 1, '2021-04-29 16:29:33', '2021-04-29 13:29:33', '', 'gallery-6', '', 'inherit', 'open', 'closed', '', 'gallery-6', '', '', '2021-04-29 16:29:33', '2021-04-29 13:29:33', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-6.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2021-04-29 16:29:33', '2021-04-29 13:29:33', '', 'gallery-7', '', 'inherit', 'open', 'closed', '', 'gallery-7', '', '', '2021-04-29 16:29:33', '2021-04-29 13:29:33', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/gallery-7.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2021-04-29 16:31:29', '2021-04-29 13:31:29', '', 'why-1', '', 'inherit', 'open', 'closed', '', 'why-1', '', '', '2021-04-29 16:31:29', '2021-04-29 13:31:29', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/why-1.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2021-04-29 16:31:29', '2021-04-29 13:31:29', '', 'why-2', '', 'inherit', 'open', 'closed', '', 'why-2', '', '', '2021-04-29 16:31:29', '2021-04-29 13:31:29', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/why-2.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2021-04-29 16:31:30', '2021-04-29 13:31:30', '', 'why-3', '', 'inherit', 'open', 'closed', '', 'why-3', '', '', '2021-04-29 16:31:30', '2021-04-29 13:31:30', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/why-3.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2021-04-29 16:31:30', '2021-04-29 13:31:30', '', 'why-4', '', 'inherit', 'open', 'closed', '', 'why-4', '', '', '2021-04-29 16:31:30', '2021-04-29 13:31:30', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/why-4.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2021-04-29 16:31:30', '2021-04-29 13:31:30', '', 'why-5', '', 'inherit', 'open', 'closed', '', 'why-5', '', '', '2021-04-29 16:31:30', '2021-04-29 13:31:30', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/why-5.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2021-04-29 16:31:31', '2021-04-29 13:31:31', '', 'why-6', '', 'inherit', 'open', 'closed', '', 'why-6', '', '', '2021-04-29 16:31:31', '2021-04-29 13:31:31', '', 5, 'http://electra:8888/wp-content/uploads/2021/04/why-6.png', 0, 'attachment', 'image/png', 0),
(86, 1, '2021-04-29 16:32:45', '2021-04-29 13:32:45', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2021-04-29 16:32:45', '2021-04-29 13:32:45', '', 5, 'http://electra:8888/?p=86', 0, 'revision', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(87, 1, '2021-04-29 17:57:22', '2021-04-29 14:57:22', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו לבן בלי טקסט', 'logo_white_single', 'publish', 'closed', 'closed', '', 'field_608ac9250d400', '', '', '2021-04-29 17:57:22', '2021-04-29 14:57:22', '', 26, 'http://electra:8888/?post_type=acf-field&p=87', 3, 'acf-field', '', 0),
(88, 1, '2021-04-29 17:58:27', '2021-04-29 14:58:27', '', 'logo-text', '', 'inherit', 'open', 'closed', '', 'logo-text', '', '', '2021-04-29 17:58:27', '2021-04-29 14:58:27', '', 0, 'http://electra:8888/wp-content/uploads/2021/04/logo-text.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2021-04-30 14:10:03', '2021-04-30 11:10:03', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'פופאפ', '%d7%a4%d7%95%d7%a4%d7%90%d7%a4', 'publish', 'closed', 'closed', '', 'group_608be55bd1262', '', '', '2021-04-30 14:10:03', '2021-04-30 11:10:03', '', 0, 'http://electra:8888/?post_type=acf-field-group&#038;p=89', 22, 'acf-field-group', '', 0),
(90, 1, '2021-04-30 14:10:03', '2021-04-30 11:10:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'pop_title', 'publish', 'closed', 'closed', '', 'field_608be561a6ce7', '', '', '2021-04-30 14:10:03', '2021-04-30 11:10:03', '', 89, 'http://electra:8888/?post_type=acf-field&p=90', 0, 'acf-field', '', 0),
(91, 1, '2021-04-30 14:10:03', '2021-04-30 11:10:03', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'pop_text', 'publish', 'closed', 'closed', '', 'field_608be566a6ce8', '', '', '2021-04-30 14:10:03', '2021-04-30 11:10:03', '', 89, 'http://electra:8888/?post_type=acf-field&p=91', 1, 'acf-field', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_termmeta`
--

CREATE TABLE `fmn_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_terms`
--

CREATE TABLE `fmn_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_terms`
--

INSERT INTO `fmn_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'כללי', '%d7%9b%d7%9c%d7%9c%d7%99', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_relationships`
--

CREATE TABLE `fmn_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_relationships`
--

INSERT INTO `fmn_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(9, 1, 0),
(15, 1, 0),
(17, 1, 0),
(25, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_taxonomy`
--

CREATE TABLE `fmn_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_taxonomy`
--

INSERT INTO `fmn_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_usermeta`
--

CREATE TABLE `fmn_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_usermeta`
--

INSERT INTO `fmn_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'dev_admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'fmn_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'fmn_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"c9d2cb91a266fdb13496ead173ecd3677f6153c38254b9f23ae6998621ec6a09\";a:4:{s:10:\"expiration\";i:1619863099;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36\";s:5:\"login\";i:1619690299;}}'),
(17, 1, 'fmn_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'fmn_user-settings', 'libraryContent=browse'),
(19, 1, 'fmn_user-settings-time', '1619700927');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_users`
--

CREATE TABLE `fmn_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_users`
--

INSERT INTO `fmn_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'dev_admin', '$P$BO4UlhgmtvehnpwxUsKuxnu8BkoEhg1', 'dev_admin', 'maxf@leos.co.il', '', '2021-04-29 09:58:11', '', 0, 'dev_admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `fmn_links`
--
ALTER TABLE `fmn_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `fmn_options`
--
ALTER TABLE `fmn_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `fmn_term_relationships`
--
ALTER TABLE `fmn_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_users`
--
ALTER TABLE `fmn_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_links`
--
ALTER TABLE `fmn_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_options`
--
ALTER TABLE `fmn_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;

--
-- AUTO_INCREMENT for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `fmn_users`
--
ALTER TABLE `fmn_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
